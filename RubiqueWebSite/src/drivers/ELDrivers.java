package drivers;

import java.awt.List;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.lang.*;
import java.util.Arrays;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class ELDrivers {
	static Workbook wb, pb;
	static Sheet ws, ws1, ws2, ws3;
	static FileInputStream filestream;
	static FileOutputStream outPutFile;
	static String flag, classname, function, parameter;
	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileIBl1 = workingDir + "/inputFiles/el_data_productflow_20170620_tb_01.xls";
	public static WebDriver driver;
	public static int methodFeild = 7;
	public static int parameterFeilds = 9;
	public static int yesflag = 8;
	public static int classnameFeild = 6;

	@Test(priority = 1)
	public static void fn_executetests1() throws Exception {
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		filestream = new FileInputStream(inputFileIBl1);
		wb = Workbook.getWorkbook(filestream);
		ws1 = wb.getSheet(1);
		System.out.println(ws1);
		for (int r = 1; r < ws1.getRows(); r++) {
			flag = ws1.getCell(yesflag, r).getContents();
			System.out.println("flag-->" + r + flag);
			if (flag.matches("yes")) {
				System.out.println("Flag is yes");
				classname = ws1.getCell(classnameFeild, r).getContents();
				System.out.println("classname-->" + classname);
				Class c = Class.forName(classname);
				System.out.println("Class-->" + c);
				function = ws1.getCell(methodFeild, r).getContents();
				parameter = ws1.getCell(parameterFeilds, r).getContents();
				System.out.println("func-->" + function);
				System.out.println("para-->" + parameter);
				Object obj = c.newInstance();
				System.out.println("funcbl-->" + function);
				System.out.println("parabl-->" + parameter);
				if ((parameter.equals("null"))) {
					System.out.println("inbl-->" + parameter);
					Method m = c.getMethod(function, null);
					m.invoke(obj, null);
				} else {
					System.out.println("out-->" + parameter);
					String stringParameters = parameter;
					String[] invokeParameterArray = stringParameters.split(",");
					System.out.println("invokeList-->" + invokeParameterArray);
					Class[] dataTypeList = new Class[invokeParameterArray.length];
					System.out.println("Before:-->" + dataTypeList);
					for (int i = 0; i < invokeParameterArray.length; i++) {
						dataTypeList[i] = String.class;
						System.out.println("in:-->" + dataTypeList[i]);
					}
					System.out.println("after-->" + dataTypeList);
					System.out.println("method-->" + function);
					Method m1 = c.getMethod(function, dataTypeList);
					System.out.println("method = " + m1.toString());
					System.out.println("obj-->" + obj);
					System.out.println("invokeList-->" + invokeParameterArray);
					m1.invoke(obj, invokeParameterArray);
					System.out.println("func-->" + function);
					System.out.print("para-->" + parameter);
					System.out.println("PASSED");
				}
			}
		}
		filestream.close();
	}

	@Test(priority = 2)
	public static void fn_executetests2() throws Exception {
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		filestream = new FileInputStream(inputFileIBl1);
		wb = Workbook.getWorkbook(filestream);
		// get TestCases sheet
		ws1 = wb.getSheet(2);
		System.out.println(ws1);
		for (int r = 1; r < ws1.getRows(); r++) {
			flag = ws1.getCell(yesflag, r).getContents();
			System.out.println("flag-->" + r + flag);

			if (flag.matches("yes")) {
				System.out.println("Flag is yes");
				classname = ws1.getCell(classnameFeild, r).getContents();
				System.out.println("classname-->" + classname);
				Class c = Class.forName(classname);
				System.out.println("Class-->" + c);
				function = ws1.getCell(methodFeild, r).getContents();
				parameter = ws1.getCell(parameterFeilds, r).getContents();
				System.out.println("func-->" + function);
				System.out.println("para-->" + parameter);
				Object obj = c.newInstance();
				System.out.println("funcbl-->" + function);
				System.out.println("parabl-->" + parameter);
				if ((parameter.equals("null"))) {
					System.out.println("inbl-->" + parameter);
					Method m = c.getMethod(function, null);
					m.invoke(obj, null);
				} else {
					System.out.println("out-->" + parameter);
					String stringParameters = parameter;
					String[] invokeParameterArray = stringParameters.split(",");
					System.out.println("invokeList-->" + invokeParameterArray);
					Class[] dataTypeList = new Class[invokeParameterArray.length];
					System.out.println("Before:-->" + dataTypeList);
					for (int i = 0; i < invokeParameterArray.length; i++) {
						dataTypeList[i] = String.class;
						System.out.println("in:-->" + dataTypeList[i]);
					}
					System.out.println("after-->" + dataTypeList);
					System.out.println("method-->" + function);
					Method m1 = c.getMethod(function, dataTypeList);
					System.out.println("method = " + m1.toString());
					System.out.println("obj-->" + obj);
					System.out.println("invokeList-->" + invokeParameterArray);
					m1.invoke(obj, invokeParameterArray);
					System.out.println("func-->" + function);
					System.out.print("para-->" + parameter);
					System.out.println("PASSED");
				}
			}
		}
		filestream.close();
	}

	@Test(priority = 3)
	public static void fn_executetests3() throws Exception {
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		filestream = new FileInputStream(inputFileIBl1);
		wb = Workbook.getWorkbook(filestream);
		ws1 = wb.getSheet(3);
		System.out.println(ws1);
		for (int r = 1; r < ws1.getRows(); r++) {
			flag = ws1.getCell(yesflag, r).getContents();
			System.out.println("flag-->" + r + flag);

			if (flag.matches("yes")) {
				System.out.println("Flag is yes");
				classname = ws1.getCell(classnameFeild, r).getContents();
				System.out.println("classname-->" + classname);
				Class c = Class.forName(classname);
				System.out.println("Class-->" + c);
				function = ws1.getCell(methodFeild, r).getContents();
				parameter = ws1.getCell(parameterFeilds, r).getContents();
				System.out.println("func-->" + function);
				System.out.println("para-->" + parameter);
				Object obj = c.newInstance();
				System.out.println("funcbl-->" + function);
				System.out.println("parabl-->" + parameter);
				if ((parameter.equals("null"))) {
					System.out.println("inbl-->" + parameter);
					Method m = c.getMethod(function, null);
					m.invoke(obj, null);
				} else {
					System.out.println("out-->" + parameter);
					String stringParameters = parameter;
					String[] invokeParameterArray = stringParameters.split(",");
					System.out.println("invokeList-->" + invokeParameterArray);

					Class[] dataTypeList = new Class[invokeParameterArray.length];

					System.out.println("Before:-->" + dataTypeList);
					for (int i = 0; i < invokeParameterArray.length; i++) {
						dataTypeList[i] = String.class;
						System.out.println("in:-->" + dataTypeList[i]);
					}
					System.out.println("after-->" + dataTypeList);
					System.out.println("method-->" + function);
					Method m1 = c.getMethod(function, dataTypeList);
					System.out.println("method = " + m1.toString());
					System.out.println("obj-->" + obj);
					System.out.println("invokeList-->" + invokeParameterArray);
					m1.invoke(obj, invokeParameterArray);
					System.out.println("func-->" + function);
					System.out.print("para-->" + parameter);
					System.out.println("PASSED");
				}
			}
		}
		filestream.close();
	}
 
	public static void main(String args[]) throws Exception {
		ELDrivers bl = new ELDrivers();
		bl.fn_executetests1();
		bl.fn_executetests2();
		bl.fn_executetests3();
	}

}
