package drivers;

import java.awt.List;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import jxl.Sheet;
import jxl.Workbook;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import utility.*;

public class InsDrivers<T> {
	static Workbook wb, pb;
	static Sheet ws, ws1, ws2, ws3;
	static FileInputStream filestream;
	static FileOutputStream outPutFile;
	static String flag, classname, function, parameter;
	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileIns1 = workingDir + "/inputFiles/ins_data_productflow_20170620_db_01.xls";
	public static WebDriver driver;
	public static String logFileXml = workingDir + "/src/utility/log4j.xml";
	public static int classnameFeild = 5;
	public static int methodFeild = 6;
	public static int yesflag = 7;
	public static int parameterFeilds = 8;

	@SuppressWarnings("deprecation")
	@Test(priority = 1)
	public static void fn_executetests() throws Exception {
		Log.startTestCase("Testing Insurance Product Flow");
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		Log.infoLog("Chrome Driver is setting done");
		filestream = new FileInputStream(inputFileIns1);
		Log.infoLog("filestream with  inputFileIns1 setting is done:" + inputFileIns1);

		wb = Workbook.getWorkbook(filestream);
		// get TestCases sheet
		ws1 = wb.getSheet(1);
		System.out.println(ws1);
		for (int r = 1; r < ws1.getRows(); r++) {
			flag = ws1.getCell(yesflag, r).getContents(); // yes or no
			System.out.println("flag-->" + r + flag);

			if (flag.matches("yes")) {
				classname = ws1.getCell(classnameFeild, r).getContents();
				System.out.println("classname-->" + classname);// cases name
				Log.infoLog("classname-->" + classname);
				Class c = Class.forName(classname);
				System.out.println("Class-->" + c);
				Log.infoLog("Class-->" + c);
				function = ws1.getCell(methodFeild, r).getContents(); // function
				parameter = ws1.getCell(parameterFeilds, r).getContents(); // Parameters
				Object obj = c.newInstance();
				System.out.println("func-->" + function);
				System.out.println("para-->" + parameter);
				Log.infoLog("func-->" + function);
				Log.infoLog("para-->" + parameter);

				if ((parameter.equals("null"))) {
					System.out.println("in-->" + parameter);
					Log.infoLog("parameter incases of null -->" + parameter);
					Log.infoLog("function in cases of null-->" + function);
					Method m = c.getMethod(function, null);
					m.invoke(obj, null);
				} else {
					System.out.println("parameter in out-->" + parameter);
					Log.infoLog("parameter in out-->" + parameter);
					String stringParameters = parameter;
					String[] invokeParameterArray = stringParameters.split(",");
					System.out.println("invokeList-->" + invokeParameterArray);
					Log.infoLog("invokeList-->" + invokeParameterArray);
					Class[] dataTypeList = new Class[invokeParameterArray.length];
					System.out.println("Before:-->" + dataTypeList);
					for (int i = 0; i < invokeParameterArray.length; i++) {
						dataTypeList[i] = String.class;
						System.out.println("in:-->" + dataTypeList[i]);
					}
					System.out.println("after-->" + dataTypeList);
					Log.infoLog("getmethod function calling:->" + function);
					Method m1 = c.getMethod(function, dataTypeList);
					System.out.println("obj-->" + obj);
					System.out.println("invokeList-->" + invokeParameterArray);
					Log.infoLog("invoke method function invokeList-->" + invokeParameterArray);
					m1.invoke(obj, invokeParameterArray);
					System.out.println("func-->" + function);
					System.out.print("para-->" + parameter);
					System.out.println("PASSED");
				}
			}
		}
		filestream.close();
		Log.endTestCase("endTestCase");
	}
 
	public static void main(String args[]) throws Exception {
		InsDrivers insurance = new InsDrivers();
		insurance.fn_executetests();
	}
}
