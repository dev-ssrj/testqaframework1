package drivers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;

import jxl.Sheet;
import jxl.Workbook;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class BaZoneDrivers {
	static Workbook wb, pb;
	static Sheet ws, ws1, ws2, ws3;
	static FileInputStream filestream;
	static FileOutputStream outPutFile;
	static String flag, classname, function, parameter;
	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileIBl1 = workingDir + "/inputFiles/ba_zone_productflow_20170620_tb_01.xls";
	public static WebDriver driver;
	public static int classnameFeild = 6;
	public static int methodFeild = 7;
	public static int yesflag = 8;
	public static int parameterFeilds = 9;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test(priority = 1)
	public static void fn_executetests1() throws Exception {
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		filestream = new FileInputStream(inputFileIBl1);
		wb = Workbook.getWorkbook(filestream);
		ws1 = wb.getSheet(1);
		System.out.println(ws1);
		for (int r = 1; r < ws1.getRows(); r++) {
			flag = ws1.getCell(yesflag, r).getContents();
			System.out.println(flag);
			if (flag.matches("yes")) {
				System.out.println("Flag is yes");
				classname = ws1.getCell(classnameFeild, r).getContents();
				System.out.println("classname-->" + classname);
				Class c = Class.forName(classname);
				System.out.println("Class-->" + c);
				function = ws1.getCell(methodFeild, r).getContents();
				parameter = ws1.getCell(parameterFeilds, r).getContents();
				System.out.println("func-->" + function);
				System.out.println("para-->" + parameter);
				Object obj = c.newInstance();
				System.out.println("funcbl-->" + function);
				System.out.println("parabl-->" + parameter);
				if ((parameter.equals("null"))) {
					System.out.println("inbl-->" + parameter);
					Method m = c.getMethod(function, null);
					m.invoke(obj, null);
				} else {
					System.out.println("out-->" + parameter);
					String stringParameters = parameter;
					String[] invokeParameterArray = stringParameters.split(",");
					System.out.println("invokeList-->" + invokeParameterArray);
					Class[] dataTypeList = new Class[invokeParameterArray.length];
					System.out.println("Before:-->" + dataTypeList);
					for (int i = 0; i < invokeParameterArray.length; i++) {
						dataTypeList[i] = String.class;
						System.out.println("in:-->" + dataTypeList[i]);
					}
					System.out.println("after-->" + dataTypeList);
					System.out.println("method-->" + function);
					Method m1 = c.getMethod(function, dataTypeList);
					System.out.println("method = " + m1.toString());
					System.out.println("obj-->" + obj);
					System.out.println("invokeList-->" + invokeParameterArray);
					m1.invoke(obj, invokeParameterArray);
				}
			}
		}
		filestream.close();
	}
	
	public static void main(String args[]) throws Exception {
		BaZoneDrivers bl = new BaZoneDrivers();
		bl.fn_executetests1();
	}
}
