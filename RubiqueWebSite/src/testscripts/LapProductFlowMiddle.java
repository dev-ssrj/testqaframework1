package testscripts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import functions.LapFunctions;


public class LapProductFlowMiddle 
{
	WebDriver driver;
	String Actualtext;
	functions.LapFunctions objectR=PageFactory.initElements(driver, LapFunctions.class);
		
	@Test
	public void fn_viewProductOffers() throws InterruptedException, ClassNotFoundException, SQLException
	{
		boolean Flag = true;
		boolean productFlag = true;
		int productFlow = 2;
		
		//driver = objectR.fn_setUpChrome();
	
		objectR.fn_openUrl("http://m.rubique.com");
			
		objectR.fn_loginButton("#log-btn-header");
		Thread.sleep(1000);	
		objectR.fn_LoginEmail("#login-email");
		Thread.sleep(1000);	
		objectR.fn_Password("#login-pwd","wasimtest");
		Thread.sleep(1000);	
		objectR.fn_loginButton("#login-button");
		
				objectR.fn_productLink("//*[@id='productsJump']/div[2]/ul/li[10]/a/p[2]");
				//objectR.fn_basicInfo();
			
				//objectR.fn_loaderwait("tr.rowShadow:nth-child(1) > td:nth-child(5) > div:nth-child(1) > button:nth-child(1)");
				//objectR.fn_productMatch("tr.rowShadow:nth-child(1) > td:nth-child(5) > div:nth-child(1) > button:nth-child(1)");
				objectR.fn_scrollPage("//*[@id='all-prods']/div/div/div[3]/div[1]/div/div/table/tbody/tr[3]/td[5]/div[1]/button");
				objectR.fn_individualProduct("//*[@id='all-prods']/div/div/div[3]/div[1]/div/div/table/tbody/tr[5]/td[5]/div[1]/button");
				objectR.fn_completeFirstStep();
				//objectR.fn_completeSecondStage();	
				Thread.sleep(1000);
			//	objectR.fn_stepTwoFullName("input[id='2'][name='2']", "BL automation user");
			 	Thread.sleep(1000);
			 	objectR.fn_stepTwoSelectGenderId("[class='form-control value-field'][column-name='gender_id'][name='7']", "Male");
			 	Thread.sleep(1000);
			 	objectR.fn_stepTwoDOB("input[id='fieldNumber8'][name='8']", "01-05-1999");
			 	Thread.sleep(1000);
				objectR.fn_stepTwoSelectMaritalStatusId("[class='form-control value-field'][column-name='marital_status_id'][name='9']", "Married");
			 	Thread.sleep(1000);
			 	objectR.fn_stepTwoSelectNationalityId("[class='form-control value-field'][column-name='nationality'][name='12']", "Indian");
			 	Thread.sleep(1000);
			 	objectR.fn_stepTwoResidentalStatus("[class ='form-control value-field'][name='14']","Resident");
			 	Thread.sleep(1000);
			 	objectR.fn_stepTwoPAN("[column-name='pan_id'][name='15']", "AKUPD2578C");
				Thread.sleep(1000);
				objectR.fn_stepTwoAddress1("[column-name='current_address.address1'][name='20']", "Sion Mumbai" );
				Thread.sleep(1000);
				objectR.fn_stepTwoAddress1("[column-name='current_address.address2'][name='21']", "andheri mumbai ");
				Thread.sleep(1000);
				objectR.fn_stepTwoState("[class ='form-control value-field'][name='24']","Maharashtra");
			//	objectR.fn_stepTwoSelectCityId("[column-name='current_city_id'][name='25']", "Pune");
				Thread.sleep(1000);
				objectR.fn_stepTwoPincode("[column-name='current_address.pincode'][name='23']", "400001");
				Thread.sleep(1000);
				objectR.fn_stepTwoYearOfResidence("[class='form-control value-field custom-class-durationy'][column-name='residence_year'][name='29']", "14");
				Thread.sleep(1000);
			
				//fn_stepTwoSelectAccomodationYearsId("//*[@id='29']", "4");
				objectR.fn_stepTwoSelectAccomodationTypeId("[class='form-control value-field'][column-name='accomodation_type'][name='30']","Owned");	
				Thread.sleep(1000);
				objectR.fn_stepOneContinueButton("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2]");
				
	    		Thread.sleep(1000);
	    		objectR.fn_completeThirdStage();	
	    		Thread.sleep(1000);
	    		objectR.fn_completeFourthStage();
	    		Thread.sleep(1000);
	    		objectR.fn_completeFifthStage();
	    		 Thread.sleep(2000);		
	    		objectR.fn_completeSixthStage(); 
	    		objectR.fn_completeSevenStageLAP();
	    		Thread.sleep(1000);
	    		objectR.fn_completeEightStage();
	    		Thread.sleep(1000);

	    		objectR.fn_stepOneContinueButton("(//*[@type='submit' and @name='submit'])[8]");
	    		
	    		
			
				
	}
	

}
