package testscripts;

import functions.TwlFunctions;

public class TwlProductFlowFppterPk1 extends TwlFunctions
{


public static void main(String[] args) throws Exception 
{
	
	TwlFunctions t1 = new TwlFunctions();
		t1.fn_setUpChrome();
		//Open URL
		t1.fn_openUrl("https://m.rubique.com/");
		//Login
		t1.fn_homeLoginClick("//*[@id='menu-item-7']/p/span");
		Thread.sleep(2000);
		t1.fn_emailId("//*[@id='login-email']", "praveen.kumar@rubique.com");
	    Thread.sleep(1000);
		t1.fn_password("//*[@id='login-pwd']", "praveen@123");
	    Thread.sleep(1000);
	    t1.fn_login("//*[@id='login-button']");
	    Thread.sleep(1000);
	    //Select TwoWheeler Loan from footer link
	    t1.fn_clickProduct("div.row:nth-child(3) > div:nth-child(1) > ul:nth-child(2) > li:nth-child(5) > a:nth-child(1)");
	    Thread.sleep(1000);
	    //SubProduct list
        t1.fn_selectTwoWheelerLoan("tr.rowShadow:nth-child(1) > td:nth-child(5) > div:nth-child(1) > a:nth-child(1)");
	    Thread.sleep(1000);
	    //Check Eligibility
        t1.fn_clickEligibility(".rb-prdct-s-instant");
	    Thread.sleep(1000);
	    //Contact Information
        t1.fn_completeFirstStepMobile("input[maxlength='10'][column-name='phone']","8600741820");
        t1.fn_completeFirstStepEmail("input[column-name='email'][type='email']","praveen.kumar@rubique.com");
		t1.fn_stepTwoSelectCity("select[column-name='current_city_id']","Mumbai");
        t1.fn_completeFirstStepContinue("(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[1]");
        Thread.sleep(1000);
        //Personal Information
        t1.fn_completeSecondStepFullName("input[id='2'][name='2']", "testerpk");
        t1.fn_stepTwoSelectGenderId("#form_group_4 > div:nth-child(2) > div:nth-child(1) > select:nth-child(2)", "Male");
        t1.fn_stepTwoSelectDOB("#fieldNumber8", "1990-06-09");
        t1.fn_stepTwoSelectMarital("#form_group_4 > div:nth-child(4) > div:nth-child(1) > select:nth-child(2)", "Married");
        t1.fn_stepTwoSelectNationality("#form_group_4 > div:nth-child(5) > div:nth-child(1) > select:nth-child(2)", "INDIAN");
        t1.fn_stepTwoSelectState("select[column-name='current_state_id']","Maharashtra");
        t1.fn_completeSecondStagePan("input[column-name='pan_id'][maxlength='10']", "DTUPK6334K");
        t1.fn_stepTwoSelectAddress_Line_1("input[column-name='current_address.address1'][maxlength='30']","test");
        t1.fn_stepTwoSelectAddress_Line_2("input[column-name='current_address.address2'][id='21']", "tester");
       // t1.fn_stepTwoSelectCity("#form_group_4 > div:nth-child(12) > div:nth-child(1) > select:nth-child(2)","Mumbai");
        t1.fn_stepTwoSelectPincode("input[id='23'][column-name='current_address.pincode']","421503");
        t1.fn_stepTwoSelectYears_at_current_residence("input[id='29'][column-name='residence_year']", "12");
        t1.fn_stepTwoSelectType_of_Accommodation("select[class='form-control value-field'][column-name='accomodation_type']","Owned");
        t1.fn_stepTwoSelectContinue("(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[2]");
        Thread.sleep(1000);
        //Loan Details
        t1.fn_completeThirdStageLoan_Amount_Required("input[id='115'][column-name='loan_amount']", "200000");
        Thread.sleep(1000);
        t1.fn_completeThirdStageTenure("(//input[@name='years'][@placeholder='Years'])[1]","4");
        Thread.sleep(1000);
        t1.fn_completeThirdStageContinue("(//button[@type='submit'][@name='submit'])[3]");
        //Applicant Information
        Thread.sleep(1000);
        t1.fn_completeFourthStageOccupation("#form_group_5 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)", "Salaried");
        Thread.sleep(1000);
        t1.fn_completeFourthStageCompany_Name("(//input[@column-name='company_id'])[1]","TATA AIG");        Thread.sleep(1000);
        t1.fn_completeFourthStageGross_Monthly_IncomeM("(//input[@id='42'][@column-name='gross_monthly_income'])[1]","80000");        
        Thread.sleep(1000);
        t1.fn_completeFourthStageType_of_Company("(//select[@class='form-control value-field'][@column-name='company_type_id'])[1]","Public Limited Company");        Thread.sleep(1000);
        t1.fn_completeFourthStageProfession_Type("(//select[@class='form-control value-field'][@column-name='profession_type_id'])[1]","Others");        Thread.sleep(1000);
        t1.fn_completeFourthStageDo_you_have_income_proof_document("(//select[@column-name='has_income_proof'][@name='50'])[1]","Yes");        Thread.sleep(1000);
        t1.fn_completeFourthStageNumber_of_Years_in_Current_Work("(//input[@column-name='current_company_experience'])[1]","7");        Thread.sleep(1000);
        t1.fn_completeFourthStageTotal_Number_of_Years_in_Work("(//input[@column-name='total_working_experience'])[1]","8");        Thread.sleep(1000);
        t1.fn_completeFourthStageContinue("(//button[@type='submit'][@name='submit'])[4]");
        Thread.sleep(1000);
        //Existing Loan Details
        t1.fn_completeFifthStageAny_Existing_Loan_with_Bank("#form_group_10 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","No");
        t1.fn_completeFifthStageContinue("(//button[@type='submit'][@name='submit'])[5]");
        Thread.sleep(2000);
      //Co-Applicant's Information
        t1.fn_AddCoapplicant("//*[@id='form_group_11']/div[1]/div/select","Yes");
        Thread.sleep(1000);
        t1.fn_RelationwithPrimaryApplicant("//*[@id='form_group_11']/div[2]/div/select","Others");
        t1.fn_CoapplicantsMonthlyincome("//*[@id='103']","850000");
        t1.fn_completeStageCoApplicantInformationNext("(//button[@type='submit'][@name='submit'])[6]");
        //Financial Information
        t1.fn_completeSixthStagePrimary_Existing_Bank_Name("#form_group_6 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)", "Axis Bank");
        t1.fn_completeSixthStageBanking_Since("(//input[@class='form-control'][@name='years'])[2]","15");
        t1.fn_completeSixthStageContinue("(//button[@type='submit'][@name='submit'])[7]");
        Thread.sleep(1000);
        //Vehicle Details
        t1.fn_completeSeventhStageManufacturer("#form_group_8 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)", "TVS");
        Thread.sleep(2000);
        t1.fn_completeSeventhStageModel("div.formOneBlock:nth-child(18) > div:nth-child(1) > select:nth-child(2)", "APPACHE RTR 160 CC");
        t1.fn_completeSeventhStageEx_showroom_Price("input[id='139'][column-name='details.ex_showroom_price']", "150000");
        t1.fn_completeSeventhStageSubmit(".submit_application_button");
        Thread.sleep(6000); 
	}

}
