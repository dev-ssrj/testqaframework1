package testscripts;

import functions.CvFunctions;


/**
 * @author praveen.kumar
 *
 */
public class CvlProductMiddlePk1 extends CvFunctions{

	public static void main(String[] args) throws InterruptedException {
	
		CvFunctions t1 = new CvFunctions();
		t1.fn_setUpChrome();
		//Open URL
		t1.fn_openUrl("https://m.rubique.com/");
		//LOGIN
				Thread.sleep(2000);

				t1.fn_homeLoginClick("//*[@id='menu-item-7']/p/span");
				Thread.sleep(2000);
				t1.fn_emailId("//*[@id='login-email']", "praveen.kumar@rubique.com");
			    Thread.sleep(1000);
				t1.fn_password("//*[@id='login-pwd']", "praveen@123");
			    Thread.sleep(1000);
			    t1.fn_login("//*[@id='login-button']");
			    Thread.sleep(1000);
			    Thread.sleep(1000);
			    t1.fn_MiddleProduct("img[src='/assets/images/websitev2/cv-icon.svg']");
			    Thread.sleep(2000);
			    t1.fn_selectFirstStageLoanAmount("input[id='visible-loan-amount']", "2000000");	   
			    Thread.sleep(1000);

		        t1.fn_selectFirstStageEmploymentType("#employment-type-group > div:nth-child(1) > div > div > label:nth-child(4)");
			    
		        Thread.sleep(2000);
		        t1.fn_selectFirstStageAnnualIncome("input[id='visible-monthly-income'][placeholder='Enter Monthly Income']","180000");
			    Thread.sleep(1000);

		        t1.fn_selectFirstStageTenure("input[name='years'][placeholder='Enter Years']", "4");
			    Thread.sleep(1000);

		        t1.fn_selectFirstStageNext("(//button[@id='search-step-one-submit-button'][@class='btn btn-next nxt-btn '])[1]");
		      
			   //Product match
		        
			    t1.fn_selectFirstStageProductMatch("button[class='apply-button purple-btn'][product-name='Manappuram Finance Commercial Vehicle Loan']");
			   
				//@Contact Information
			   	Thread.sleep(1000);
			   	t1.fn_selectSecondStagemobile("input[class='form-control value-field custom-class-mobile'][column-name='phone']","8600741820");
			   	t1.fn_selectSecondStageEmailId("input[class='form-control value-field custom-class-email'][ui-type='email']","praveen.kumar@rubique.com");
			    t1.fn_stepTwoSelectCity("select[column-name='current_city_id'][class='form-control value-field']","Mumbai");
			   	Thread.sleep(1000);

			    t1.fn_selectSecondStageNext("(//button[@type='submit'][@name='submit'])[1]");
			    //@Personal Information
			    
			   	Thread.sleep(1000);
			    t1.fn_completeSecondStepFullName("input[id='2'][name='2']", "testerpk");
			    t1.fn_stepTwoSelectGenderId("select[column-name='gender_id'][ui-type='dropdown']","Male");
			   	Thread.sleep(1000);

			    t1.fn_stepTwoSelectDOB("input[id='fieldNumber8'][column-name='dob']", "1990-06-09");
			    t1.fn_stepTwoSelectMarital("select[column-name='marital_status_id'][class='form-control value-field']","Married");
			    t1.fn_completeSecondStagePan("input[column-name='pan_id'][maxlength='10']", "DTUPK6334K");
			    t1.fn_stepTwoSelectAddressLine1("input[column-name='current_address.address1'][maxlength='30']","test");
			    t1.fn_stepTwoSelectAddressLine2("input[column-name='current_address.address2'][id='21']", "tester");
			    t1.fn_stepTwoSelectYearsatcurrentresidence("input[id='29'][column-name='residence_year']", "12");
			    t1.fn_stepTwoSelectTypeofAccommodation("select[class='form-control value-field'][column-name='accomodation_type']", "Owned");
			    t1.fn_stepTwoSelectContinue("(//button[@type='submit'][@name='submit'])[2]");
			    
			    
			    //Loan Details
			    Thread.sleep(1000);
		        t1.fn_completeThirdStageLoan_Amount_Required("input[id='115'][column-name='loan_amount']","1500000");
		        Thread.sleep(1000);

		        t1.fn_completeThirdStageTenure("(//input[@class='form-control'][@name='years'])[1]", "7");
		        Thread.sleep(1000);

		        t1.fn_completeThirdStageContinue("(//button[@type='submit'][@name='submit'])[3]");
		        Thread.sleep(3000);
		        
		        t1.fn_completeFourthStageOccupation("select[class='form-control value-field'][column-name='occupation_id']", "Self Employed");
		        Thread.sleep(1000);
		        t1.fn_completeFourthStageCompanyName("div.parent-id-38:nth-child(12) > div:nth-child(1) > input:nth-child(2)","TATA AIG");

		        t1.fn_completeFourthStageApplicantType("select[class='form-control value-field'][column-name='other_details.applicant_type']","Individual");
		        Thread.sleep(1000);

		        t1.fn_completeFourthStageLastYearAnnualIncome("input[id='44'][column-name='annual_income.annual_income_year_1']", "180000");
		        Thread.sleep(1000);

		        t1.fn_completeFourthStageConstitution("select[name='52'][column-name='other_details.constitution']","Others");
		        Thread.sleep(1000);
		        t1.fn_completeFourthStageAnnualIncomeYr2("input[id='45'][column-name='annual_income.annual_income_year_2']","1800000"); 
		        t1.fn_completeFourthStageTotalYearsinCurrentBusiness("input[id='61'][column-name='current_company_experience']","5"); 
		        
		        t1.fn_completeFourthStageTotalYearsinOverallBusiness("input[id='63'][column-name='total_working_experience']","5"); 

		       
		        t1.fn_completeFinalSubmit(".submit_application_button");
		        Thread.sleep(6000);
	}

}
