package testscripts;
import functions.ClFunctions;

public class ClHeaderProductFlowPk1 {
	
	public static void main(String[] args) throws Exception 
	{
		ClFunctions t1 = new ClFunctions();
		
		t1.fn_setUpChrome();
		t1.fn_openUrl("https://m.rubique.com/");
		
		t1.fn_homeLoginClick("//*[@id='menu-item-7']/p/span");
		Thread.sleep(2000);
		t1.fn_emailId("//*[@id='login-email']","wasim.sheikh@rubique.com");
	    Thread.sleep(1000);
		t1.fn_password("//*[@id='login-pwd']","wasimtest");
	    Thread.sleep(1000);
	    t1.fn_login("//*[@id='login-button']");
	    
	    //Product selection 
	    Thread.sleep(4000);
        t1.fn_clickProduct("li.ubermenu-item-object-page:nth-child(2) > a:nth-child(1) > span:nth-child(1)");
	    Thread.sleep(1000);

        t1.fn_ProductCarloan("li.ubermenu-tab:nth-child(2) > a:nth-child(1) > span:nth-child(1)");
	    Thread.sleep(1000);

	    t1.fn_ProductCarlloanAuSmallFinance("li.ubermenu-item-type-taxonomy:nth-child(13) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1) > span:nth-child(1)");
	    Thread.sleep(1000);
	    //Eligibility 
        t1.fn_clickEligibility(".rb-prdct-s-instant");
	    Thread.sleep(1000);
	    
	  //Contact informationn
	    t1.fn_selectSecondStagemobile("input[id='5'][column-name='phone']", "8600741820");
	    t1.fn_selectSecondStageEmailId("input[id='6'][column-name='email']", "praveen.kumar@rubique.com");
        t1.fn_stepTwoSelectCity("select[column-name='current_city_id']","Mumbai");

	    t1.fn_selectSecondStageNext("(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[1]");
	    Thread.sleep(2000);
	    t1.fn_completeSecondStepFullName("input[id='2'][name='2']", "testerpk");
        t1.fn_stepTwoSelectGenderId("#form_group_4 > div:nth-child(2) > div:nth-child(1) > select:nth-child(2)", "Male");
        t1.fn_stepTwoSelectDOB("#fieldNumber8", "1990-06-09");
        t1.fn_stepTwoSelectMarital("#form_group_4 > div:nth-child(4) > div:nth-child(1) > select:nth-child(2)", "Single");
        t1.fn_stepTwoSelectNationality("#form_group_4 > div:nth-child(5) > div:nth-child(1) > select:nth-child(2)", "INDIAN");
        t1.fn_completeSecondStagePan("input[column-name='pan_id'][maxlength='10']","DTUPK6334P");
        t1.fn_stepTwoSelectAddress_Line_1("input[column-name='current_address.address1'][maxlength='30']","test");
        t1.fn_stepTwoSelectAddress_Line_2("input[column-name='current_address.address2'][id='21']","tester");
        t1.fn_stepTwoState("select[column-name='current_state_id']","Maharashtra");
        //t1.fn_stepTwoSelectCity("#form_group_4 > div:nth-child(12) > div:nth-child(1) > select:nth-child(2)","Mumbai");
        t1.fn_stepTwoSelectPincode("input[id='23'][column-name='current_address.pincode']","421503");
        t1.fn_stepTwoSelectYears_at_current_residence("input[id='29'][column-name='residence_year']","12");
        t1.fn_stepTwoSelectType_of_Accommodation("select[class='form-control value-field'][column-name='accomodation_type']","Owned");
        t1.fn_stepTwoSelectContinue("(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[2]");
        Thread.sleep(1000);
        t1.fn_completeThirdStageLoan_Amount_Required("input[id='115'][column-name='loan_amount']","1500000");
        Thread.sleep(1000);
        t1.fn_completeThirdStageTenure("(//input[@name='years'][@placeholder='Years'])[1]","4");
        Thread.sleep(1000);
        t1.fn_completeThirdStageContinue("(//button[@type='submit'][@name='submit'])[3]");
        Thread.sleep(1000);
        t1.fn_completeFourthStageOccupation("#form_group_5 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Salaried");
        t1.fn_completeFourthStageCompany_Name("(//input[@column-name='company_id'])[1]","TATA AIG");
        t1.fn_completeFourthStageGross_Monthly_Income("(//input[@id='42'][@column-name='gross_monthly_income'])[1]","80000");
        t1.fn_completeFourthStageDo_you_have_income_proof_document("(//select[@column-name='has_income_proof'][@name='50'])[1]","Yes");
        t1.fn_completeFourthStageNumber_of_Years_in_Current_Work("//*[@id='60']","7");
        t1.fn_completeFourthStageTotal_Number_of_Years_in_Work("//*[@id='62']","7");
        t1.fn_completeFourthStageContinue("(//button[@type='submit'][@name='submit'])[4]");
        Thread.sleep(1000);
        t1.fn_completeFifthStageAny_Existing_Loan_with_Bank("#form_group_10 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","No");
        t1.fn_completeFifthStageContinue("(//button[@type='submit'][@name='submit'])[5]");
        Thread.sleep(1000);
        t1.fn_completeSixthStageBanking_Since("(//input[@class='form-control'][@name='years'])[2]","15");
        t1.fn_completeSixthStageContinue("(//button[@type='submit'][@name='submit'])[6]");
        Thread.sleep(2000);
       // t1.fn_completeSeventhStageVehicle_Type("#form_group_8 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Honda");
        t1.fn_completeSeventhStageManufacturer("#form_group_8 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Honda");        Thread.sleep(2000);

        t1.fn_completeSeventhStageModel("#form_group_8 > div.formOneBlock.col-xs-12.col-sm-6.col-md-6.col-lg-6.parent-id-137.parent-option-id-2000015 > div > select","City VX");        Thread.sleep(2000);

        t1.fn_completeSeventhStageEx_showroom_Price("input[id='139'][column-name='details.ex_showroom_price']","1500000");        Thread.sleep(2000);

        //t1.fn_completeSeventhStageAgeofVehicle("//*[@id='140']","0");
        t1.fn_completeFinalSubmit(".submit_application_button");
        Thread.sleep(6000);
	

	}

}
