package testscripts;
import org.openqa.selenium.WebDriver;

import functions.*;
public class PlProductFlowmiddleBk1 extends PlFunctions  {
	public static WebDriver driver;

	public static void main(String[] args) throws Exception 
	{
		PlFunctions t1 = new PlFunctions();
		driver= t1.fn_setUpChrome();
		t1.fn_openUrl("https://m.rubique.com/");
         Thread.sleep(1000);
         t1.fn_homeLoginClick("#log-btn-header");
         Thread.sleep(1500);
         t1.fn_emailId("#login-email", "wasim.sheikh@rubique.com");
         Thread.sleep(1500);
         t1.fn_password("#login-pwd", "wasimtest");
         Thread.sleep(1500);
         t1.fn_login("button[id=\"login-button\"][type=\"button\"]");
         Thread.sleep(7000);

   t1.fn_middlePL("(//p[@class='prod-name-home'])[2]");
	 Thread.sleep(2000);
	 t1.fn_scrollPage("(//button[@class='hex-hor-button btn-md col-sm-6'])[1]");
	 t1.fn_selectPl("button[product-name='Go upward Personal Loan']");
	 Thread.sleep(2000);
	 //t1.fn_checkEligibility(".rb-prdct-s-instant");
	 Thread.sleep(2000);
	 t1.fn_completeSecondStepFullName("input[column-name='name']","Bhupendra");//full name
	 //Thread.sleep(2000);
	 //t1.fn_completeFirstStepMobile("input[id='5'][column-name='phone']","9665377429");
	 Thread.sleep(2000);
	 //t1.fn_completeFirstStepEmail("input[id=\"6\"][column-name]","praveen.kumar@rubique.com");
	 //Thread.sleep(2000);
	 t1.stepTwoSelectCity("//select[@column-name='current_city_id']","Mumbai");
	 t1.fn_completeFirstStepContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[1]");
	 Thread.sleep(3000);
	 //t1.fn_completeSecondStepFullName("input[id=\"2\"][column-name]","Bhupendra");//full name
	 Thread.sleep(2000);
	 t1.fn_stepTwoSelectGenderId("//select[@column-name='gender_id']", "Male"); // gender male
	 t1.stepTwoSelectDOB("#fieldNumber8","1990-06-09");  // DOB
	 t1.stepTwoSelectMarital("select[column-name='marital_status_id']","Single");  // unmarried
	 t1.fn_stepTwoSelectNationality("select[column-name='nationality']","Indian"); // nationality
	 t1.fn_stepTwoHighest_Qualification("//select[@column-name='highest_qualification']", "High School");
	 t1.fn_completeSecondStagePan("input[id=\"15\"][column-name=\"pan_id\"]", "cdnpp8846b");
	 t1.fn_stepTwoSelectAddress_Line1("input[id=\"20\"][name=\"20\"]", "abcd");
	 t1.fn_StepTwoSelectAddress_Line2("input[id=\"21\"][name=\"21\"]", "xyz");
	 t1.fn_StepTwoSelectLandmark("//*[@id=\"22\"]","Ram Nagar");
	 t1.fn_SelectState("//select[@column-name='current_state_id']", "Bihar");
	 t1.stepTwoSelectPincode("//*[@id=\"23\"]","425401");
	 t1.stepTwoSelectYears_at_Current_Residence("//*[@id=\"29\"]","12");
	 t1.stepTwoTypeOfAccomodation("//select[@column-name='accomodation_type']","Owned");
	 t1.stepTwoSelectContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[2]");
	 Thread.sleep(2000);
	 t1.fn_stepLoanAmountRequired("//*[@id=\"115\"]","1250000");
	 t1.fn_stepTenure("//*[@id=\"unified-inputs\"]/input[1]","10");
	 t1.fn_purposeOfLoan("//*[@id=\"form_group_2\"]/div[3]/div/select","Business");
	 t1.fn_scrollPage("//*[@id=\"form_group_2\"]/div[1]/div[1]/label");
	 t1.fn_stepSelectContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[3]");
	 Thread.sleep(2000);
	 
   t1.fn_completeFourthStageOccupation("//*[@id=\"form_group_5\"]/div[1]/div/select","Salaried");
   t1.fn_completeFourthStageCompany_Name("//*[@id=\"fieldNumber40\"]","TATA AIG");
   t1.fn_completeFourthStageGross_Monthly_Income("//*[@id=\"42\"]", "80000");
   t1.fn_completeFourthStageNet_Monthly_Income("//*[@id=\"43\"]","120000");
   t1.fn_completeForthStageModeOF_salary("//*[@id=\"form_group_5\"]/div[5]/div/select","Cash Payment");
   t1.fn_completeTypeOFCompany("//*[@id=\"form_group_5\"]/div[6]/div/select", "Partnership");
   t1.fn_forthStageProfessionType("//*[@id=\"form_group_5\"]/div[7]/div/select","Others");
   t1.fn_forthStageDesignation("//*[@id=\"form_group_5\"]/div[8]/div/select","Senior level");
   Thread.sleep(1000);
   //t1.fn_completeFourthStageDo_you_have_income_proof_document("div.parent-id-38:nth-child(4) > div:nth-child(1) > select:nth-child(2)", 1);
   t1.fn_completeFourthStageNumber_of_Years_in_Current_Work("(//input[@id=\"60\"][@column-name=\"current_company_experience\"])[1]","10");
   Thread.sleep(1000);
   t1.fn_completeFourthStageTotal_Number_of_Years_in_Work("(//input[@id=\"62\"][@column-name=\"total_working_experience\"])[1]", "11");
   Thread.sleep(1000);
   t1.fn_completeForthStageOffice_Address1("(//input[@id=\"70\"][@column-name=\"office_address.address1\"])[1]","Saki naka");
   Thread.sleep(1000);
   t1.fn_completeForthStageOffice_Address2("(//input[@id=\"71\"][@class=\"form-control value-field custom-class-text\"])[1]", "krislon house");
   Thread.sleep(1000);
   t1.fn_completeForthStageOffice_city("//*[@id=\"form_group_5\"]/div[14]/div/select","Bangalore");
 Thread.sleep(1000);
   t1.fn_completeForthStageOffice_Pincode("//*[@id=\"75\"]","425401");
   Thread.sleep(1000);
   t1.fn_completeForthStageOffice_Phone("//*[@id=\"78\"]","025512522");
   Thread.sleep(2000);
   t1.fn_scrollPage("//*[@id=\"form_group_5\"]/div[14]/div/label");
   t1.fn_completeFourthStageContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[4]");
  Thread.sleep(2000);
  t1.fn_completeFifthStageAny_ExistingLoanWithBank("//*[@id=\"form_group_10\"]/div[1]/div/select","No");
  t1.fn_completeFifthStageContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[5]");
   t1.fn_completeSixthStagePrimaryExistingBank_Name("//*[@id=\"form_group_6\"]/div[1]/div/select","Allahabad Bank");
   t1.fn_completeSixthStageBankAccountType("//select[@column-name='account_type']", "Savings");
   t1.fn_completeSixthStageAnyChequeBounceInLast_ThreeMonths("//select[@column-name='cheque_bounce']","No");
   t1.fn_completeFinalSubmit(".submit_application_button");
   
  
	}

	
	
}
