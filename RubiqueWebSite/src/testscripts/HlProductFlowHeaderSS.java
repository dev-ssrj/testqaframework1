package testscripts;

import functions.HlFunctions;

public class HlProductFlowHeaderSS extends HlFunctions  {

	public static void main(String[] args) throws Exception {
	
	// Open Website
	fn_openWebsite("https://m.rubique.com/");
		
	// Login 
	fn_login("span[id='log-btn-header']");
	fn_loginUsername("input[id='login-email']","wasim.sheikh@rubique.com");
	fn_loginPassword("input[id='login-pwd'][class='form-control'][name='pass']","wasimtest");
	fn_loginClick("button[id='login-button']");
		
	// Header 
	fn_headerhlflowclick("Retail","Consumer Loans","HDFC Ltd");
	fn_checkEligibility("a[class='rb-prdct-s-instant check-eligibility-product-btn']");
		
	// Contact Information
	fn_steptwo_FullName("input[id='2'][name='2']","Sangeetha");
	fn_steponeMobile("input[class='form-control value-field custom-class-mobile']","9004");
	//fn_steponeEmailId("input[class='form-control value-field custom-class-email']","wasim.sheikh@rubique.com");
	fn_city("select[column-name='current_city_id'][name='25']","2");
	fn_cssContinue("button[class='hex-hor-button continue_button pull-right']");
			
	// Applicant Information
	
	fn_steptwo_Gender("select[column-name='gender_id'][name='7']","2");
	fn_steptwo_dob("input[id='fieldNumber8'][name='8']","1988-01-01");
	fn_steptwo_MaritalStatus("select[column-name='marital_status_id'][name='9']","2");
	fn_steptwo_Nationality("select[column-name='nationality'][name='12']","2");
	fn_steptwo_PAN("input[column-name='pan_id'][name='15']","CNJPS7878J");
	fn_steptwo_PassportNo("input[column-name='passport'][name='16']","P1234567");
	fn_steptwo_AadhaarNo("input[column-name='aadhaar_id'][name='17']","123456789012");
	fn_steptwo_AddressLine1("input[column-name='current_address.address1'][name='20']","AddressLine1");
	fn_steptwo_AddressLine2("input[column-name='current_address.address2'][name='21']","AddressLine12");
	fn_steptwo_Landmark("input[column-name='current_address.address3'][name='22']","Landmark");
	fn_steptwo_Pincode("input[column-name='current_address.pincode'][name='23']","401105");
	fn_steptwo_LandlineNumber("input[column-name='landline_no.landline_1'][name='27']","49204920");
	fn_steptwo_YearsCurrentResidence("input[column-name='residence_year'][name='29']","25");
	fn_steptwo_TypeofAccommodation("select[column-name='accomodation_type'][name='30']","2");
	/* fn_steptwo_permanentaddressLine1("input[id='31'][name='31']", "permanentAddressLine1");
	fn_steptwo_permanentAddressLine2("input[id='32'][name='32']", "permanentAddressLine2");
	fn_steptwo_permanentlandmark3("input[id='33'][name='33']", "permanentlandmark3");
	fn_steptwo_permanentCity("select[name='35']", "1" ); 
	fn_steptwo_permanentPincode("input[id='36'][name='36']", "102233"); */
	fn_xpathContinue("html/body/section[3]/section/div[2]/div/div[2]/div[2]/form/div[30]/button[2]");
	                  

	// Loan Details 
	fn_stepthree_loanAmountRequired("input[class='form-control value-field custom-class-formatted-number slider-inp-value'][name='115']","5000000");
	fn_stepthree_tenureYears("input[class='form-control'][name='years'][placeholder='Years']","10");
	fn_stepthree_tenureMonths("input[class='form-control'][name='months'][placeholder='Months']","5");
	fn_scrollPage("//input[@column-name='loan_amount']");
	fn_xpathContinue("(//*[@name='submit' and @class='hex-hor-button continue_button pull-right'])[3]");
						 
	// Applicant Information
	fn_stepfour_Occupation("select[class='form-control value-field'][column-name='occupation_id'][name='38']","1");
	fn_stepfour_sserCompanyName("input[name='40'][column-name='company_id']","GOOGLE PRIVATE LIMITED");
	fn_stepfour_srGrossMonthlyIncome("input[id='42'][column-name='gross_monthly_income']","200000");
	//fn_stepfour_seLastYearAnnualIncome("input[id='44'][name='44']","20000000");
	//fn_stepfour_seAnnualIncomeYr2("input[id='45'][name='45']","20000000");
	//fn_stepfour_seConstitution("select[class='form-control value-field'][column-name='other_details.constitution'][name='217']","1");
	fn_stepfour_sModeofSalary("select[class='form-control value-field'][column-name='salary_mode_id'][name='84']","1");
	fn_stepfour_sTypeofCompany("select[class='form-control value-field'][column-name='company_type_id'][name='51']","1");
	fn_stepfour_sseProfessionType("select[class='form-control value-field'][column-name='profession_type_id']","1");
	fn_stepfour_sseNumberofYearsinCurrentWork("input[id='60'][class='form-control value-field custom-class-durationy'][name='60']","10");
	fn_stepfour_sseTotalNumberofYearsinWork("input[id='62'][class='form-control value-field custom-class-durationy'][name='62']","12");
	//fn_stepfour_seFinancialsAudited("select[class='form-control value-field'][column-name='other_details.financials_audited'][name='69']","1");
	fn_xpathContinue("html/body/section[3]/section/div[2]/div/div[2]/div[4]/form/div[19]/button[2]");
	fn_closePopup("html/body/section[6]/div/div/div/div[1]/button");
			
		// Existing Loan Details 
			
		fn_stepfive_anyExistingLoan("select[class='form-control value-field'][name='86']","1");
		fn_stepfive_existingLoanEMI("input[id='90'][name='90']","2");
		fn_xpathContinue("html/body/section[3]/section/div[2]/div/div[2]/div[5]/form/div[3]/button[2]");
		fn_closePopup("html/body/section[6]/div/div/div/div[1]/button");
			
		// Co-Applicant's Information
		fn_stepsix_AddCoapplicant("select[name='96'][column-name='is_coapplicant']","2");
		/* fn_stepsix_relationWithPrimaryApplicant("select[name='97'][column-name='customer_relationship_type']","Father");
		fn_stepsix_coapplicantName("input[name='98'][id='98']", "coapplicantName");
		fn_stepsix_coapplicantDOB("input[name='99'][id='fieldNumber99']","1988-01-12");
		// fn_stepsix_coapplicantDOBmonth("select[class='ui-datepicker-month']");
		// fn_stepsix_coapplicantDOByear("select[class='ui-datepicker-year']");
		fn_stepsix_coapplicantEmail("input[name='100'][id='100']","coapplicantEmail@email.com");
		fn_stepsix_coapplicantMobile("input[name='101'][id='101']","9112233445");
		fn_stepsix_coapplicantOccupation("select[name='102']","Salaried");
		fn_stepsix_coapplicantMonthlyIncome("input[name='103'][id='103']","200000");
		fn_stepsix_coapplicantExistingLoan("select[name='107']","Yes");
		fn_stepsix_coapplicantExistingEMI("input[name='108']","2"); */
			
		fn_xpathContinue("html/body/section[3]/section/div[2]/div/div[2]/div[6]/form/div[12]/button[2]");
		fn_closePopup("html/body/section[6]/div/div/div/div[1]/button");
			
		// Financial Information
			
		fn_stepseven_BankAccountType("select[name='81']","1");
		fn_stepseven_BankingSinceYears("html/body/section[3]/section/div[2]/div/div[2]/div[7]/form/div[2]/div/div/input[1]","10");
		fn_stepseven_BankingSinceMonths("html/body/section[3]/section/div[2]/div/div[2]/div[7]/form/div[2]/div/div/input[2]","10");
		fn_stepseven_AnyChequeBounce("select[name='94']","2");
		//fn_stepseven_haveCurrentAccountsincelastoneyear("select[name='190']","Yes");
		fn_xpathContinue("html/body/section[3]/section/div[2]/div/div[2]/div[7]/form/div[4]/button[2]");
		fn_closePopup("html/body/section[6]/div/div/div/div[1]/button");
			

    // PropertyDetails
			
	/* xpath fn_stepeight_TypeofProperty("html/body/section[2]/section/div[2]/div/div[2]/div[8]/form/div[1]/div/select",1);
	 css fn_stepeight_ClassificationofProperty("select[name='122']",1);
     css fn_stepeight_PurposeofLoan("select[name='123']");
     css fn_stepeight_PropertyName("input[name='134'][id='134']");
	 css fn_stepeight_BuilderDeveloper("input[name='129'][id='129']");
	 css fn_stepeight_PropertyCity("input[id='fieldNumber133'][name='133'][api-source='1001']"); */
		
	fn_stepeight_TypeofProperty("select[name='121']","1");
	
		
	// Type of Property : Residential : 1	
	fn_stepeight_ClassificationofProperty("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[2]/div/select","1");
	fn_stepeight_PurposeofLoan("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[3]/div/select","1");
		
	/*// Type of Property : Plot : 2	
	fn_stepeight_TypeofProperty("select[name='121']","2");
	fn_stepeight_ClassificationofProperty("html/body/section[2]/section/div[2]/div/div[2]/div[8]/form/div[4]/div/select","2");
    fn_stepeight_PurposeofLoan("html/body/section[2]/section/div[2]/div/div[2]/div[8]/form/div[5]/div/select","2");
		
	//fn_stepeight_StatusofProperty("select[name='124']",1); */
	fn_stepeight_StatusofProperty("select[name='124']","2");
	/*  fn_stepeight_StatusofProperty("select[name='124']","3");
	 ////fn_stepeight_StatusofProperty("html/body/section[2]/section/div[2]/div/div[2]/div[8]/form/div[6]/div/select",2);  */
	
	//Status of Property : 2 : Ready		
	fn_stepeight_PropertyName("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[7]/div/input","Name ReadyPossession");
	fn_stepeight_BuilderDeveloper("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[8]/div/input","Builder ReadyPossession");
	fn_stepeight_PropertyCity("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[9]/div/select","2"); 
	                            
	/* //Status of Property : 3 : Under	
	fn_stepeight_PropertyName("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[10]/div/input","Name UnderConstruction");
	fn_stepeight_BuilderDeveloper("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[11]/div/input","Builder UnderConstruction");
	fn_stepeight_PropertyCity("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[12]/div/input","3"); */
	
	fn_stepeight_MarketValueOfProperty("input[id='126'][name='126']","8000000");
	fn_stepeight_Submit("html/body/section[3]/section/div[2]/div/div[2]/div[8]/form/div[14]/button[2]");
	//fn_stepeight_Submit("button[class='hex-hor-button submit_application_button pull-right'][name='submit']");
	//fn_closePopup("html/body/section[6]/div/div/div/div[1]/button");
	//fn_clickOutsidepopup("html/body/section[6]/div");
	//html/body/a/i
	
			
	// Lead Information 
	fn_TrackingID("html/body/section[4]/div[1]/div[2]/span[1]");
    //fn_TrackingID("html/body/section[2]/section/div/div/div[1]/div/div/fieldset[4]/section[2]/div[1]/div[2]/span[1]");
	fn_closeDriver();



	}

}
