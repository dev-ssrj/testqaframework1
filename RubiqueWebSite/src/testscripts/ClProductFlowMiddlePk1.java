package testscripts;

import functions.ClFunctions;

public class ClProductFlowMiddlePk1 extends ClFunctions {

	public static void main(String[] args) throws Exception {
		ClProductFlowMiddlePk1 t1 = new ClProductFlowMiddlePk1();
		driver = t1.fn_setUpChrome();
		t1.fn_openUrl("https://m.rubique.com/");

		// LOGIN
		t1.fn_homeLoginClick("//*[@id='menu-item-7']/p/span");
		Thread.sleep(2000);
		t1.fn_emailId("//*[@id='login-email']","praveen.kumar@rubique.com");
		Thread.sleep(1000);
		t1.fn_password("//*[@id='login-pwd']","praveen@123");
		Thread.sleep(1000);
		t1.fn_login("//*[@id='login-button']");
		Thread.sleep(8000);
		// @SLECTED CAR LOAN FROM MIDDLE OF THE WEB
		t1.fn_clickProduct("img[src='/assets/images/websitev2/cl-icon.svg']");
		
		// @BASIC INFO
		Thread.sleep(3000);
		t1.fn_selectFirstStageLoanAmount("#visible-loan-amount", "1000000");
		Thread.sleep(1000);

        //t1.fn_selectFirstStageEmploymentType("label.btn:nth-child(3)");
        t1.fn_selectFirstStageCompanyOrganization("#current-company", "TATA AIG");
		Thread.sleep(1000);

        t1.fn_selectFirstStageMonthlyIncome("#visible-monthly-income", "100000");
		Thread.sleep(1000);

	    t1.fn_selectFirstStageTenure("input.fieldNumber:nth-child(1)", "3");
		Thread.sleep(1000);

	    t1.fn_selectFirstStageNext("#search-step-one-submit-button");
		Thread.sleep(1000);
		t1.fn_selectFirstStageProductMatch("tr.rowShadow:nth-child(1) > td:nth-child(5) > div:nth-child(1) > button:nth-child(1)");
		Thread.sleep(2000);

		t1.fn_selectSecondStagemobile("input[id='5'][column-name='phone']", "8600741820");
		t1.fn_selectSecondStageEmailId("input[id='6'][column-name='email']", "praveen.kumar@rubique.com");
		t1.fn_stepTwoSelectCity("select[column-name='current_city_id']","Mumbai");
		t1.fn_selectSecondStageNext("(//button[@type='submit'][@name='submit'])[1]");

		// Contact_information click continue button
		Thread.sleep(2000);
		 t1.fn_completeSecondStepFullName("input[id='2'][name='2']", "testerpk");
	        t1.fn_stepTwoSelectGenderId("#form_group_4 > div:nth-child(2) > div:nth-child(1) > select:nth-child(2)", "Male");
	        t1.fn_stepTwoSelectDOB("#fieldNumber8", "1990-06-09");
	        t1.fn_stepTwoSelectMarital("#form_group_4 > div:nth-child(4) > div:nth-child(1) > select:nth-child(2)", "Single");
	        t1.fn_stepTwoSelectNationality("#form_group_4 > div:nth-child(5) > div:nth-child(1) > select:nth-child(2)", "INDIAN");
	        t1.fn_completeSecondStagePan("input[column-name='pan_id'][maxlength='10']","DTUPK6334P");
	        t1.fn_stepTwoSelectAddress_Line_1("input[column-name='current_address.address1'][maxlength='30']","test");
	        t1.fn_stepTwoSelectAddress_Line_2("input[column-name='current_address.address2'][id='21']","tester");
	        t1.fn_stepTwoState("select[column-name='current_state_id']","Maharashtra");
	        //t1.fn_stepTwoSelectCity("#form_group_4 > div:nth-child(12) > div:nth-child(1) > select:nth-child(2)","Mumbai");
	        t1.fn_stepTwoSelectPincode("input[id='23'][column-name='current_address.pincode']","421503");
	        t1.fn_stepTwoSelectYears_at_current_residence("input[id='29'][column-name='residence_year']","12");
	        t1.fn_stepTwoSelectType_of_Accommodation("select[class='form-control value-field'][column-name='accomodation_type']","Owned");
	        t1.fn_stepTwoSelectContinue("(//button[@class='hex-hor-button continue_button pull-right'][@type='submit'])[2]");
	        Thread.sleep(1000);
	        t1.fn_completeThirdStageLoan_Amount_Required("input[id='115'][column-name='loan_amount']","1500000");
	        Thread.sleep(1000);
	        t1.fn_completeThirdStageTenure("(//input[@name='years'][@placeholder='Years'])[1]","4");
	        Thread.sleep(1000);
	        t1.fn_completeThirdStageContinue("(//button[@type='submit'][@name='submit'])[3]");
	        Thread.sleep(1000);
	        t1.fn_completeFourthStageOccupation("#form_group_5 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Salaried");
	        t1.fn_completeFourthStageCompany_Name("(//input[@column-name='company_id'])[1]","TATA AIG");
	        t1.fn_completeFourthStageGross_Monthly_Income("(//input[@id='42'][@column-name='gross_monthly_income'])[1]","80000");
	        t1.fn_completeFourthStageDo_you_have_income_proof_document("(//select[@column-name='has_income_proof'][@name='50'])[1]","Yes");
	        t1.fn_completeFourthStageNumber_of_Years_in_Current_Work("//*[@id='60']","7");
	        t1.fn_completeFourthStageTotal_Number_of_Years_in_Work("//*[@id='62']","7");
	        t1.fn_completeFourthStageContinue("(//button[@type='submit'][@name='submit'])[4]");
	        Thread.sleep(1000);
	        t1.fn_completeFifthStageAny_Existing_Loan_with_Bank("#form_group_10 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","No");
	        t1.fn_completeFifthStageContinue("(//button[@type='submit'][@name='submit'])[5]");
	        Thread.sleep(1000);
	        t1.fn_completeSixthStageBanking_Since("(//input[@class='form-control'][@name='years'])[2]","15");
	        t1.fn_completeSixthStageContinue("(//button[@type='submit'][@name='submit'])[6]");
	        Thread.sleep(2000);
	       // t1.fn_completeSeventhStageVehicle_Type("#form_group_8 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Honda");
	        t1.fn_completeSeventhStageManufacturer("#form_group_8 > div:nth-child(1) > div:nth-child(1) > select:nth-child(2)","Honda");        Thread.sleep(2000);

	        t1.fn_completeSeventhStageModel("#form_group_8 > div.formOneBlock.col-xs-12.col-sm-6.col-md-6.col-lg-6.parent-id-137.parent-option-id-2000015 > div > select","City VX");        Thread.sleep(2000);

	        t1.fn_completeSeventhStageEx_showroom_Price("input[id='139'][column-name='details.ex_showroom_price']","1500000");        Thread.sleep(2000);

	        //t1.fn_completeSeventhStageAgeofVehicle("//*[@id='140']","0");
	        t1.fn_completeFinalSubmit(".submit_application_button");
	        Thread.sleep(6000);
		

	}

}
