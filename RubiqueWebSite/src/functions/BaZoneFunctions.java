package functions;

/** 
 * Author: Trupti Bhosale created on 2017-06-17
 * This file contains all functions for all fields needed for executing Business loan product flow
 * */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class BaZoneFunctions {
	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileIBl1 = workingDir + "/RubiqueWebSite/inputFiles/BLtestdataFooterFlow.xls";
	public static WebDriver driver;
	boolean present;
	String Actualtext, Actualtext1;
	String LaonAmount, years, months, companName, occupation;
	// int Income;
	public String locatorValue;
	String dataValue;
	boolean productFlag = true;
	static String emailid;
	String mobileNumber1;
	public static Workbook wb;
	public static Sheet ws;
	public static Sheet ws1;
	public static Sheet ws2;
	public static Sheet ws3;
	public static FileInputStream fs;
	// public locatorValue;
	String winHandleBefore;
	String subWindowHandler = null;
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public static String screenSnapsPath = workingDir + "/outputFiles/" + "baz" + timeStamp + ".png";

	public static void fn_takeScreenshot(WebDriver driver, String path) {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fn_takeScreenshot taken check at: " + path);
	}

	public BaZoneFunctions(WebDriver BLDriver) {

		// this.driver=BLDriver;
		BlFunctions.driver = driver;
	}

	public BaZoneFunctions() {
	}

	public WebDriver fn_setUpChrome() {

		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);

		// Initialize browser
		driver = new ChromeDriver();

		// wait = new WebDriverWait(driver, 5);
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver;
	}

	public void fn_openUrl(String URL) {

		fn_setUpChrome();
		driver.get(URL);
		// Maximize browser
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("fn_openUrl");

	}

	public void fn_scrollPage(String locator) {
		WebElement element = driver.findElement(By.cssSelector(locator));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

	}

	public void fn_productLink(String locator) throws InterruptedException {

		WebElement productlink = driver.findElement(By.xpath(locator));
		productlink.click();
		System.out.println("fn_productLink");

	}

	public void fn_stepOneFirstName(String locator, String name) throws InterruptedException {

		locator = locator.replaceAll("^\"|\"$", "");
		name = name.replaceAll("^\"|\"$", "");
		Thread.sleep(1000);
		WebElement FirstName = driver.findElement(By.cssSelector(locator));
		FirstName.sendKeys(name);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoFirstName");
	}

	public void fn_stepOneLastName(String locator, String name) throws InterruptedException {

		locator = locator.replaceAll("^\"|\"$", "");
		name = name.replaceAll("^\"|\"$", "");
		Thread.sleep(1000);
		WebElement LastName = driver.findElement(By.cssSelector(locator));
		LastName.sendKeys(name);
		Thread.sleep(1000);
		System.out.println("fn_stepTwoLastName");
	}

	public void fn_stepOneMobileNumber(String locator, String mobileNumber) throws InterruptedException {
		Thread.sleep(3000);
		mobileNumber = mobileNumber.replaceAll("^\"|\"$", "");
		WebElement mbNumber = driver.findElement(By.cssSelector(locator));
		mbNumber.sendKeys(mobileNumber);
		System.out.println("fn_stepOneMobileNumber");
	}

	public void fn_stepOneEmail(String locator) throws InterruptedException {

		emailid = randomEmail();
		locator = locator.replaceAll("^\"|\"$", "");
		WebElement emailIds = driver.findElement(By.cssSelector(locator));
		emailIds.sendKeys(emailid);
		System.out.println("fn_stepOneEmail");
	}

	public static String randomEmail() {
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(1000);
		return "username" + randomInt + "@gmail.com";
	}

	public void fn_stepOneSelectCity(String locator, String City) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		City = City.replaceAll("^\"|\"$", "");
		WebElement Cities = driver.findElement(By.cssSelector(locator));
		Cities.sendKeys(City);
		System.out.println("fn_stepOneCity");
	}

	public void fn_stepOneProfessionType(String locator, String Profession) throws InterruptedException {
		locator = locator.replaceAll("^\"|\"$", "");
		Profession = Profession.replaceAll("^\"|\"$", "");
		WebElement Professions = driver.findElement(By.cssSelector(locator));
		Professions.sendKeys(Profession);
		System.out.println("fn_stepOneProfessions");
	}

	public void fn_stepOneSubmitButton(String locator) {
		locator = locator.replaceAll("^\"|\"$", "");
		fn_takeScreenshot(driver, screenSnapsPath);
		WebElement submitButton = driver.findElement(By.xpath(locator));
		submitButton.click();
		System.out.println("fn_stepOneContinueButton");
	}

	public void fn_getWindowHandle() {
		// Store the current window handle
		winHandleBefore = driver.getWindowHandle();

	}

	public void fn_switchWindow() {
		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

	}

	public void fn_switchPopUpWindow() {
		Set<String> handles = driver.getWindowHandles(); // get all window
															// handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window

	}

	public void fn_closePopUp(String locator) throws InterruptedException {
		Thread.sleep(5000);
		WebElement closePopUp = driver.findElement(By.xpath(locator));
		closePopUp.click();
	}

	public void fn_switchWindowclose() {
		// Close the new window, if that window no more required
		driver.close();
	}

	public void fn_switchOriginalWindow() {
		// Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

	}

}
