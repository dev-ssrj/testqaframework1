package functions;

import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import drivers.ClDrivers;

public class ClFunctions extends ClDrivers {

	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileCL1 = workingDir
			+ "/RubiqueWebSite/inputFiles/cl_data_productflow_20170620_pk_01.xls";
	public static WebDriver driver;

	public WebDriver fn_setUpChrome()
	{
		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver;
	}

	public void fn_closeBrowser()
	{
		System.out.println("closing browser fn_closeBrowser");
		driver.close();
	}
	
	public void fn_openUrl(String URL) {
		driver.manage().window().maximize();
		driver.get(URL);
		System.out.println("fn_openUrl");
	}

	public void fn_homeLoginClick(String locator) throws InterruptedException {
		WebElement homeLoginClick = driver.findElement(By.xpath(locator));
		homeLoginClick.click();
		System.out.println("fn_homeLoginClick");
	}

	public void fn_emailId(String locator, String email) throws InterruptedException {
		Thread.sleep(2000);
		WebElement emailid = driver.findElement(By.xpath(locator));
		emailid.sendKeys(email);
		System.out.println("fn_emailId");
	}

	public void fn_password(String locator, String pss) throws InterruptedException {
		Thread.sleep(2000);

		WebElement passwrd = driver.findElement(By.xpath(locator));
		passwrd.sendKeys(pss);
		System.out.println("Pass fn_password");
	}

	public void fn_login(String string) throws InterruptedException {
		WebElement login = driver.findElement(By.xpath(string));
		login.click();
		login.click();
		System.out.println("Logged In");
		Thread.sleep(2000);

	}

	public void fn_clickProduct(String locator) throws InterruptedException {
		Thread.sleep(2000);
		WebElement clickProduct = driver.findElement(By.cssSelector(locator));
		clickProduct.click();
		System.out.println("CarFlow");
	}

	public void fn_selectCarLoan(String Aucar_loan) throws InterruptedException {
		Thread.sleep(4000);
		WebElement selectCarLoan = driver.findElement(By.xpath(Aucar_loan));
		selectCarLoan.click();
		System.out.println("Clicked Carloan");
	}

	public void fn_clickEligibility(String Aucar_loan) throws InterruptedException {
		Thread.sleep(4000);
		WebElement clickEligibility = driver.findElement(By.cssSelector(Aucar_loan));
		clickEligibility.click();
		System.out.println("Product Aucar_loan");
	}

	public void fn_completeFirstStepMobile(String locator, String Mobile) throws InterruptedException {
		Thread.sleep(2000);
		WebElement completeFirstStepMobile = driver.findElement(By.cssSelector(locator));
		completeFirstStepMobile.sendKeys(Mobile);
		System.out.println("Pass MobileNo.");
	}

	public void fn_completeFirstStepEmail(String locator, String Email) throws InterruptedException {
		Thread.sleep(2000);
		WebElement completeFirstStepEmail = driver.findElement(By.cssSelector(locator));
		completeFirstStepEmail.sendKeys(Email);
		System.out.println("Pass Email");
	}

	public void fn_completeFirstStepContinue(String click) {
		WebElement completeFirstStepContinue = driver.findElement(By.xpath(click));
		completeFirstStepContinue.click();
		System.out.println("fn_completeFirstStep Completed");
	}

	public void fn_completeSecondStepFullName(String locator, String pass) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeSecondStepFullName = driver.findElement(By.cssSelector(locator));
		completeSecondStepFullName.sendKeys(pass);
		System.out.println("Pass FullName");
	}

	public void fn_stepTwoSelectGenderId(String locator, String gender) throws InterruptedException {
		Thread.sleep(1000);
		Select drp0 = new Select(driver.findElement(By.cssSelector(locator)));
		drp0.selectByVisibleText(gender);
		System.out.println("Pass GenderId");
	}
	
	public void fn_stepTwoSelectDOB(String locator, String pss) throws InterruptedException {
		Thread.sleep(1000);

		WebElement stepTwoSelectDOB = driver.findElement(By.cssSelector(locator));
		stepTwoSelectDOB.sendKeys(pss);
		System.out.println("Pass DOB");
	}

	public void fn_stepTwoSelectMarital(String locator, String Marital) throws InterruptedException {
		Thread.sleep(1000);

		Select drp1 = new Select(driver.findElement(By.cssSelector(locator)));
		drp1.selectByVisibleText(Marital);
		System.out.println("Pass MaritalStatus");
	}

	public void fn_stepTwoSelectNationality(String locator, String Nationality) throws InterruptedException {
		Thread.sleep(1000);

		Select drp2 = new Select(driver.findElement(By.xpath(locator)));
		drp2.selectByVisibleText(Nationality);
		System.out.println("Pass Nationality");
	}

	public void fn_completeSecondStagePan(String locator, String pann) throws InterruptedException {
		Thread.sleep(1000);

		WebElement completeSecondStagePan = driver.findElement(By.cssSelector(locator));
		completeSecondStagePan.sendKeys(pann);
		System.out.println("Pass PanID");
	}

	public void fn_stepTwoSelectAddress_Line_1(String locator, String pss) throws InterruptedException {
		Thread.sleep(1000);

		WebElement stepTwoSelectAddress_Line_1 = driver.findElement(By.cssSelector(locator));
		stepTwoSelectAddress_Line_1.sendKeys(pss);
		System.out.println("Pass Address1");
	}

	public void fn_stepTwoSelectAddress_Line_2(String locator, String pss) {
		WebElement stepTwoSelectAddress_Line_2 = driver.findElement(By.cssSelector(locator));
		stepTwoSelectAddress_Line_2.sendKeys(pss);
		System.out.println("Pass Address2");
	}

	public void fn_stepTwoSelectCity(String locator, String Owned) throws InterruptedException {
		Thread.sleep(1000);

		Select drp3 = new Select(driver.findElement(By.cssSelector(locator)));
		drp3.selectByVisibleText(Owned);
		System.out.println("Pass CityName");
	}

	public void fn_stepTwoState(String locator, String Occupation) throws InterruptedException {
		Thread.sleep(1000);
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Occupation);
		System.out.println("Pass Occupation");
	}
	public void fn_stepTwoSelectPincode(String locator, String pss) throws InterruptedException {
		Thread.sleep(1000);

		WebElement stepTwoSelectPincode = driver.findElement(By.cssSelector(locator));
		stepTwoSelectPincode.sendKeys(pss);
		System.out.println("Pass PinCode");
	}

	public void fn_stepTwoSelectYears_at_current_residence(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);

		WebElement stepTwoSelectPincode = driver.findElement(By.cssSelector(locator));
		stepTwoSelectPincode.sendKeys(ps);
		System.out.println("Pass YearOfCurrentResidence");
	}
	public void fn_stepTwoSelectState(String locator, String Owned) {
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Owned);
		System.out.println("fn_stepTwoSelectType_of_Accommodation");
	}
	public void fn_stepTwoSelectType_of_Accommodation(String locator, String Owned) throws InterruptedException {
		Thread.sleep(1000);

		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Owned);
		System.out.println("Pass TypeOfAccommadation");
	}

	public void fn_stepTwoSelectContinue(String locator) {
		WebElement stepTwoSelectPincode = driver.findElement(By.xpath(locator));
		stepTwoSelectPincode.click();
		System.out.println("fn_stepTwoSelect Completed");
	}

	public void fn_completeThirdStageLoan_Amount_Required(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
		completeThirdStageLoan_Amount_Required.clear();
		completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("Pass LoanAmountRequired");
	}

	public void fn_completeThirdStageTenure(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);

		WebElement completeThirdStageTenure = driver.findElement(By.xpath(locator));
		completeThirdStageTenure.clear();
		completeThirdStageTenure.sendKeys(ps);
		System.out.println("Pass Tenure");
	}

	public void fn_completeThirdStageContinue(String locator) {
		WebElement completeThirdStageContinue = driver.findElement(By.xpath(locator));
		completeThirdStageContinue.click();
		System.out.println("fn_completeThirdStage Completed");
	}

	public void fn_completeFourthStageOccupation(String locator, String Occupation) throws InterruptedException {
		Thread.sleep(3000);
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Occupation);
		System.out.println("Pass Occupation");
	}

	public void fn_completeFourthStageCompany_Name(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);

		WebElement completeFourthStageCompany_Name = driver.findElement(By.xpath(locator));
		completeFourthStageCompany_Name.clear();
		completeFourthStageCompany_Name.sendKeys(ps);
		System.out.println("Pass CompanyName");
	}

	public void fn_completeFourthStageGross_Monthly_Income(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeFourthStageGross_Monthly_Income = driver.findElement(By.xpath(locator));
		completeFourthStageGross_Monthly_Income.clear();
		completeFourthStageGross_Monthly_Income.sendKeys(ps);
		System.out.println("Pass GrossMonthlyIncome");
	}

	public void fn_completeFourthStageDo_you_have_income_proof_document(String locator, String Owned) throws InterruptedException {
		Thread.sleep(1000);
		Select drp5 = new Select(driver.findElement(By.xpath(locator)));
		drp5.selectByVisibleText(Owned);
		System.out.println("Pass IncomeProofDoc");
	}

	public void fn_completeFourthStageNumber_of_Years_in_Current_Work(String locator, String work) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeFourthStageNumber_of_Years_in_Current_Work = driver.findElement(By.xpath(locator));
		completeFourthStageNumber_of_Years_in_Current_Work.clear();
		completeFourthStageNumber_of_Years_in_Current_Work.sendKeys(work);
		System.out.println("Pass NumberOfYearsInCurrentWork");
	}

	public void fn_completeFourthStageTotal_Number_of_Years_in_Work(String locator, String TotalWork) throws InterruptedException {
		Thread.sleep(1000);

		WebElement completeFourthStageTotal_Number_of_Years_in_Work = driver.findElement(By.xpath(locator));
		completeFourthStageTotal_Number_of_Years_in_Work.clear();
		completeFourthStageTotal_Number_of_Years_in_Work.sendKeys(TotalWork);
		System.out.println("Pass TotalNoOfYearsInWork");
	}

	public void fn_completeFourthStageContinue(String locator) throws InterruptedException {
		Thread.sleep(1000);

		WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
		completeFourthStageContinue.click();
		System.out.println("fn_completeFourthStage Completed");
	}

	public void fn_completeFifthStageAny_Existing_Loan_with_Bank(String locator, String No)throws InterruptedException {
		Thread.sleep(2000);
		Select drp6 = new Select(driver.findElement(By.cssSelector(locator)));
		drp6.selectByVisibleText(No);
		System.out.println("Pass Any_Existing_Loan_with_Bank");
	}

	public void fn_completeFifthStageContinue(String locator) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
		Thread.sleep(2000);
		completeFourthStageContinue.click();
		System.out.println("fn_completeFifthStage Completed");
	}

	public void fn_completeSixthStageBanking_Since(String locator, String Banking) throws InterruptedException {
		Thread.sleep(2000);
		WebElement completeSixthStageBanking_Since = driver.findElement(By.xpath(locator));
		completeSixthStageBanking_Since.sendKeys(Banking);
		System.out.println("Pass BankingSince");

	}

	public void fn_completeSixthStageContinue(String locator) throws InterruptedException {
		WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
		completeFourthStageContinue.click();
		System.out.println("fn_completeSixthStage Completed");
	}

	public void fn_completeSeventhStageManufacturer(String locator, String Honda) throws InterruptedException {
		Thread.sleep(4000);
		Select drp8 = new Select(driver.findElement(By.cssSelector(locator)));
		drp8.selectByVisibleText(Honda);
		System.out.println("Pass Manufacturer");
	}

	public void fn_completeSeventhStageModel(String locator, String Model) throws InterruptedException {
		Thread.sleep(2000);
		Select drp8 = new Select(driver.findElement(By.cssSelector(locator)));
		drp8.selectByVisibleText(Model);
		System.out.println("Pass Model");
	}

	public void fn_completeSeventhStageEx_showroom_Price(String locator, String Showroom_Price) throws InterruptedException {
		Thread.sleep(2000);
		WebElement completeSeventhStageEx_showroom_Price = driver.findElement(By.cssSelector(locator));
		completeSeventhStageEx_showroom_Price.sendKeys(Showroom_Price);
		System.out.println("Pass ExShowroomPrice");
	}

	public void fn_completeSeventhStageAgeofVehicle(String locator, String Showroom_Price) throws InterruptedException {
		Thread.sleep(1000);
		WebElement AgeofVehicle = driver.findElement(By.xpath(locator));
		AgeofVehicle.sendKeys(Showroom_Price);
		System.out.println("Pass ExShowroomPrice");
	}

	public void fn_completeFinalSubmit(String locator) throws InterruptedException {
		Thread.sleep(3000);
		WebElement completeFinalSubmit = driver.findElement(By.cssSelector(locator));
		completeFinalSubmit.click();
		System.out.println("fn_completeFinalSubmit Completed");
	}

	// Header flow fuctions

	
	public void fn_ProductH(String locator) throws InterruptedException{
		Thread.sleep(5000);
		WebElement subMenu = driver.findElement(By.cssSelector(locator));
		subMenu.click();
		System.out.println("fn_Product");
		}
	
	public void fn_ProductTWLheader(String carloan) throws InterruptedException{
		Thread.sleep(3000);
		WebElement subMenu = driver.findElement(By.cssSelector(carloan));
		Actions act =new Actions(driver);
	    act.moveToElement(subMenu);
	    act.moveToElement(subMenu).build().perform();
	    System.out.println("fn_ProductTWLheader");
		}
	public void fn_ProductCarloanAuBank(String carloan) throws InterruptedException{
		Thread.sleep(3000);
			WebElement subMenu = driver.findElement(By.cssSelector(carloan));
			subMenu.click(); 
			System.out.println("fn_ProductCarloanHDFCBank");
			}
	public void fn_selectCarLoanM(String locator) throws InterruptedException {
		Thread.sleep(3000);
		WebElement selectCarLoan1 = driver.findElement(By.xpath(locator));
		selectCarLoan1.click();

	}

	public void fn_completeFirstStepLoanAmount(String locator, String amount) {
		WebElement LoanAmount = driver.findElement(By.cssSelector(locator));
		LoanAmount.sendKeys(amount);
	}

	public void fn_completeFirstStepEmploymentType(String locator) {
		WebElement EmploymentType = driver.findElement(By.cssSelector(locator));
		EmploymentType.click();
	}

	public void fn_completeFirstStepCompany_Organization(String locator, String companyname) {
		WebElement CompanyOrganization = driver.findElement(By.cssSelector(locator));
		CompanyOrganization.sendKeys(companyname);
	}

	public void fn_completeFirstStepMonthly_Income(String locator, String Income) {
		WebElement Monthly_Income = driver.findElement(By.cssSelector(locator));
		Monthly_Income.sendKeys(Income);
	}

	public void fn_completeFirstStepTenure(String locator, String years) {
		WebElement Tenure = driver.findElement(By.cssSelector(locator));
		Tenure.sendKeys(years);
	}

	public void fn_completeFirstStepNext(String locator) {
		WebElement Next = driver.findElement(By.cssSelector(locator));
		Next.click();
	}

	public void fn_completeFirstStepProductMatch(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		/*
		 * examining the xpath for a search box
		 */
		driver.findElement(By.cssSelector(locator)).click();
	}

	public void fn_completeThirdStageContinue1(String locator) {
		WebElement completeThirdStageContinue1 = driver.findElement(By.cssSelector(locator));
		completeThirdStageContinue1.click();
	}

	/*
	 * MIDDLE FLOW
	 * 
	 */
	public void fn_selectFirstStageProductMatch(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		driver.findElement(By.cssSelector(locator)).click();
		System.out.println("fn_selectFirstStageProductMatch");
	}
	public void fn_selectFirstStageNext(String locator) throws InterruptedException {
		WebElement next = driver.findElement(By.cssSelector(locator));
		next.click();
		Thread.sleep(1000);
		System.out.println("fn_selectFirstStageNext");
	}
	public void fn_selectFirstStageTenure(String locator, String years) {

		WebElement Tenure = driver.findElement(By.cssSelector(locator));
		Tenure.sendKeys(years);
		System.out.println("fn_selectFirstStageTenure");
	}
	public void fn_selectFirstStageMonthlyIncome(String locator, String income) {

		WebElement login = driver.findElement(By.cssSelector(locator));
		login.sendKeys(income);
		System.out.println("fn_selectFirstStageMonthlyIncome");
	}
	public void fn_selectFirstStageLoanAmount(String locator, String amount) throws InterruptedException {
        Thread.sleep(1000);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.clear();
		login.sendKeys(amount);
		System.out.println("fn_selectFirstStageLoanAmount");

	}	
	public void fn_selectFirstStageCompanyOrganization(String locator, String companyName) throws InterruptedException {
        Thread.sleep(1000);
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.sendKeys(companyName);
		System.out.println("fn_selectFirstStageCompanyOrganization");
	}
	public void fn_Product(String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		// *examining the css for a search
		driver.findElement(By.cssSelector(locator)).click();
		System.out.println("HeaderDropDownProduct");

	}

	public void fn_ProductCarloan(String carloan) {
		WebElement subMenu = driver.findElement(By.cssSelector(carloan));
		Actions act = new Actions(driver);
		act.moveToElement(subMenu);
		act.moveToElement(subMenu).build().perform();
		System.out.println("CarLoan");

	}

	public void fn_ProductCarlloanAuSmallFinance(String carloan) throws InterruptedException {
		Thread.sleep(2000);
		WebElement subMenu = driver.findElement(By.cssSelector(carloan));
		subMenu.click();
		System.out.println("Selected AuSmallFinance");

	}

	public void fn_selectSecondStagemobile(String locator, String mobile) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		driver.findElement(By.cssSelector(locator));
		WebElement Tenure = driver.findElement(By.cssSelector(locator));
		Tenure.clear();
		Tenure.sendKeys(mobile);
		System.out.println("Pass Mobile");

	}
	public void fn_selectSecondStageEmailId(String locator, String email) {

		WebElement emailid = driver.findElement(By.cssSelector(locator));
		emailid.clear();
		emailid.sendKeys(email);
		System.out.println("Pass emailid");

	}

	public void fn_selectSecondStageNext(String locator) {
		WebElement next = driver.findElement(By.xpath(locator));
		next.click();
		System.out.println("fn_selectSecondStage Completed");

	}

	public void fn_stepTwoSelectGenderId(String locator, int gender) throws InterruptedException {
		Select drp0 = new Select(driver.findElement(By.cssSelector(locator)));
		drp0.selectByIndex(gender);
		System.out.println("Pass Gender");

	}

	public void fn_stepTwoSelectMarital(String locator, int Marital) {
		Select drp1 = new Select(driver.findElement(By.cssSelector(locator)));
		drp1.selectByIndex(Marital);
		System.out.println("Pass MaritalStatus");

	}

	public void fn_stepTwoSelectNationality(String locator, int Nationality) {
		Select drp2 = new Select(driver.findElement(By.cssSelector(locator)));
		drp2.selectByIndex(Nationality);
		System.out.println("Pass Nationality");

	}

	public void fn_stepTwoSelectCity(String locator, int Owned) {
		Select drp3 = new Select(driver.findElement(By.cssSelector(locator)));
		drp3.selectByIndex(Owned);
		System.out.println("Pass CityName");
	}

	public void fn_stepTwoSelectType_of_Accommodation(String locator, int Owned) {
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByIndex(Owned);
		System.out.println("Pass Type_of_Accommodation");

	}

	public void fn_completeFourthStageDo_you_have_income_proof_document(String locator, int Owned) {
		Select drp10 = new Select(driver.findElement(By.cssSelector(locator)));
		drp10.selectByIndex(Owned);
		System.out.println("Pass fn_completeFourthStageDo_you_have_income_proof_document");
	}

	public void fn_completeFifthStageAny_Existing_Loan_with_Bank(String locator, int No) {
		Select drp6 = new Select(driver.findElement(By.cssSelector(locator)));
		drp6.selectByIndex(No);
		System.out.println("Pass fn_completeFourthStageDo_you_have_income_proof_document");
	}

	public void fn_completeSeventhStageVehicleType(String locator, int pss) {
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByIndex(pss);
		System.out.println("Pass fn_completeFourthStageDo_you_have_income_proof_document");
	}

	public void fn_completeSeventhStageManufacturer(String locator, int pss) {
		Select drp20 = new Select(driver.findElement(By.cssSelector(locator)));
		drp20.selectByIndex(pss);
		System.out.println("Pass fn_completeFourthStageDo_you_have_income_proof_document");
	}

	public void fn_completeSeventhStageModel(String locator, int pss) {
		Select drp20 = new Select(driver.findElement(By.cssSelector(locator)));
		drp20.selectByIndex(pss);
		System.out.println("Pass fn_completeFourthStageDo_you_have_income_proof_document");
	}

	public void fn_completeSeventhStageYearofManufacture(String locator, String pss) {
		WebElement YearofManufacture = driver.findElement(By.cssSelector(locator));
		YearofManufacture.sendKeys(pss);
		System.out.println("Pass fn_completeFourthStageDo_you_have_income_proof_document");
	}

	public void fn_completeFinalStage(String locator) {
		WebElement completeFinalStage = driver.findElement(By.cssSelector(locator));
		completeFinalStage.click();
		System.out.println("Pass fn_completeFourthStageDo_you_have_income_proof_document");
		
	}
	public void fn_scrollPage(String locator) throws InterruptedException {
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.cssSelector(locator));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		}
	public void fn_scrollpage()
	{
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,250)", "");
	}

}
