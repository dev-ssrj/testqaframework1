package functions;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.BrowserVersion;

public class HlFunctions {
	
   
   static FileInputStream fs ;
   static XSSFWorkbook wb ;
   static XSSFSheet s1 , s2 , s3 , s4 ;
   static String workingDir = System.getProperty("user.dir");
    public static WebDriver  driver;
    //public static WebDriver  driver;
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public static String screenSnapsPath = workingDir +"/outputFiles/"+"hl"+timeStamp+".png";
	
  
	/*public  static  void fn_takeScreenshot(WebDriver driver, String path) throws InterruptedException {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fn_takeScreenshot taken check at: "+path);
		Thread.sleep(5000);
	} 
	
	public static HtmlUnitDriver  fn_setUpHtmlUnitDriver()
	{
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver = new HtmlUnitDriver ( BrowserVersion.CHROME );
			System.out.println("fn_setUpChrome");
			return (HtmlUnitDriver) driver;
	} */
	
	public static WebDriver fn_setupChromeDriver() 	{   
		System.setProperty("webdriver.chrome.driver",workingDir+"/utility/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		System.out.println("fn_setUpChrome");
		return driver; 	}
	
	/* public static WebDriver fn_setupChromeDriver() 	{   
		System.setProperty("webdriver.chrome.driver",workingDir+"/utility/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		System.out.println("fn_setUpChrome");
		return driver; 	} */

	public static void fn_openWebsite(String url) throws Exception {
		fn_setupChromeDriver();
	    //fn_setUpHtmlUnitDriver();
		driver.navigate().to(url);
		Thread.sleep(2000);
		System.out.println("fn_openWebsite");
	}
	
	public static void fn_Home(String locator ) throws InterruptedException {
		Thread.sleep(2000);
		WebElement Home = driver.findElement(By.cssSelector(locator));
		// WebElement Home = driver.findElement(By.xpath(locator));
		Home.click();
		Thread.sleep(2000);
		System.out.println(driver.getCurrentUrl());
	    System.out.println("fn_Home");
	    }
	
	
	public static  void fn_refresh() throws Exception {
		driver.navigate().refresh();
		Thread.sleep(2000);
		
	}
	
	
	public static  void fn_clear(String locator) throws Exception {
		WebElement clearbox = driver.findElement(By.xpath(locator));
		clearbox.clear();
		Thread.sleep(2000);
		
	}
	
	public static  void fn_closePopup(String locator) throws Exception {
		WebElement closePopup = driver.findElement(By.xpath(locator));
        if((closePopup).isDisplayed()){
		closePopup.click(); }
		Thread.sleep(5000);
		System.out.println("fn_closePopup");
	}
	
	public static  void fn_clickOutsidepopup(String locator) throws Exception {
		Thread.sleep(5000);
		WebElement clickOutsidepopup = driver.findElement(By.xpath(locator));
		clickOutsidepopup.click();
		Thread.sleep(5000);
		System.out.println("fn_clickOutsidepopup");
	}
	
	public static void fn_login(String locator) {
		WebElement login = driver.findElement(By.cssSelector(locator));
		login.click();
				}
	
	public static void fn_loginUsername(String locator , String Username) throws InterruptedException {
		Thread.sleep(2000);
		WebElement loginUsername = driver.findElement(By.cssSelector(locator));
		loginUsername.sendKeys(Username);
			}
	
	public static void fn_loginPassword(String locator , String Password) throws InterruptedException {
		Thread.sleep(2000);
		WebElement loginPassword = driver.findElement(By.cssSelector(locator));
		loginPassword.sendKeys(Password);
		Thread.sleep(2000);
			}
	
	public static void fn_loginClick(String locator) throws InterruptedException {
		WebElement loginClick = driver.findElement(By.cssSelector(locator));
		loginClick.click();
		System.out.println(driver.getCurrentUrl());
		Thread.sleep(5000);
		System.out.println("fn_loginClick");
		}
	
	public static void fn_headerhlflowclick(String mainProd, String subProd, String prod) throws InterruptedException {
		
		System.out.println(mainProd + subProd + prod);
		// Default set the locator
		if (mainProd.equals(null)) {
			System.out.println(" mainProd is null ");
			mainProd = "Retail";
		}
		if (subProd.equals(null)) {
			System.out.println(" subProd is null ");
			subProd = "Consumer Loans";
		}
		if (prod.equals(null)) {
			System.out.println(" subProd is null ");
			prod = "HDFC Ltd";
		}
		
		// step 1
		Actions actionProduct = new Actions(driver);
		WebElement c1 = driver.findElement(By.partialLinkText(mainProd));
		if (c1.isDisplayed()) {
			System.out.println("Element1 is Visible");
			actionProduct.clickAndHold(c1).perform();
			Thread.sleep(4000);
		} else {
			System.out.println("Element1 is InVisible");
		}

		// step 2
		WebElement c2 = driver.findElement(By.linkText(subProd));
		if (c2.isDisplayed()) {
			System.out.println("Element2 is Visible");
			actionProduct.clickAndHold(c2).perform();
			Thread.sleep(5000);
		} else {
			System.out.println("Element2 is InVisible");
		}
		// step 3
		WebElement c3 = driver.findElement(By.partialLinkText(prod));
		if (c3.isDisplayed()) {
			System.out.println("Element3 is Visible");
			actionProduct.click(c3).perform();
			Thread.sleep(4000);
		} else {
			System.out.println("Element3 is InVisible");
		}

		System.out.println("fn_mainDropDownProduct");
	}
	
	public static void fn_middlehlflowclick(String locator) throws InterruptedException {
		Thread.sleep(5000);
	WebElement middleIcon = driver.findElement(By.cssSelector(locator));
	 middleIcon.click();
	 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 System.out.println(driver.getCurrentUrl());
	 System.out.println("fn_middlehlflowclick"+driver.getCurrentUrl());
	 Thread.sleep(5000);
	}
	
	public static void fn_footerhlflowclick(String locator) {
	WebElement footerlink = driver.findElement(By.cssSelector(locator));
	footerlink.click();
	 System.out.println(driver.getCurrentUrl());
	 System.out.println("fn_footerhlflowclick");
	}
	
	public static void fn_goToTop(String locator) throws InterruptedException  {
		driver.findElement(By.cssSelector(locator)).click();
		Thread.sleep(2000);
		
	}
	
	public static void fn_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		driver.findElement(By.linkText(locator)).click();
		Thread.sleep(2000);
		System.out.println("fn_Continue");
	}
	
	
	public static void fn_stepone_Continue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		driver.findElement(By.cssSelector(locator)).click();
		Thread.sleep(2000);
		System.out.println("fn_stepone_Continue");
	}
	
	public static void fn_xpathContinue(String locator) throws InterruptedException  {
		//fn_takeScreenshot(driver, screenSnapsPath);
		Thread.sleep(5000);
		driver.findElement(By.xpath(locator)).click();
		Thread.sleep(10000);
		System.out.println("fn_xpathContinue");
	}
	

	
	public static void fn_Previous(String locator) throws InterruptedException  {
		driver.findElement(By.cssSelector(locator)).click();
		Thread.sleep(2000);
		System.out.println("fn_stepsix_Continue");
	}
	
	public static void fn_mBILoanAmount(String locator , String LoanAmount) throws InterruptedException {
		//Thread.sleep(5000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement mBILoanAmount = driver.findElement(By.cssSelector(locator));
		mBILoanAmount.sendKeys(LoanAmount);
		System.out.println("fn_mBILoanAmount");
	}
	// not working	
	public static void fn_mBILoanAmountinwords(String locator) throws InterruptedException {
			WebElement mBILoanAmountinwords = driver.findElement(By.cssSelector(locator));
			System.out.println(mBILoanAmountinwords.getText());
			System.out.println("fn_mBILoanAmountinwords");
			 	}
	// not working
	public static void fn_mBIEmploymentType(String locator) throws InterruptedException {
		Thread.sleep(2000);
		WebElement mBIemploymentType = driver.findElement(By.cssSelector(locator));
		mBIemploymentType.click();
		System.out.println("fn_mBIEmploymentType");
		 	}
	
	public static void fn_mBICompany(String locator,String cmpnyname) throws InterruptedException {
		WebElement company = driver.findElement(By.cssSelector(locator));
		company.sendKeys(cmpnyname);
		System.out.println("fn_mBICompany");
		 	}
	
	public static void fn_mBIMonthlyIncome(String locator,String incomeamount) throws InterruptedException {
		WebElement monthlyIncome = driver.findElement(By.cssSelector(locator));
		monthlyIncome.sendKeys(incomeamount);
		System.out.println("fn_mBIMonthlyIncome");
		 	}
	// not working
	public static void fn_mBIMonthlyIncomeinwords(String locator) throws InterruptedException {
		WebElement monthlyIncomeinwords = driver.findElement(By.cssSelector(locator));
		System.out.println(monthlyIncomeinwords.getText());
		System.out.println("fn_mBIMonthlyIncomeinwords");
		}
	
	public static void fn_mBITenureyears(String locator,String years) throws InterruptedException {
		WebElement monthlyIncome = driver.findElement(By.cssSelector(locator));
		monthlyIncome.sendKeys(years);
		System.out.println("fn_mBITenureyears");
		}
	
	public static void fn_mBITenuremonths(String locator,String months) throws InterruptedException {
		WebElement monthlyIncome = driver.findElement(By.cssSelector(locator));
		monthlyIncome.sendKeys(months);
		System.out.println("fn_mBITenuremonths"); 	}

	public static void fn_mBINext(String locator) throws InterruptedException {
		WebElement mbinext = driver.findElement(By.cssSelector(locator));
		mbinext.click();
		Thread.sleep(2000);
		System.out.println("fn_mBINext");
	}
	
	public static void fn_apply(String locator) throws InterruptedException {
		Thread.sleep(5000);
		WebElement apply = driver.findElement(By.cssSelector(locator));
		apply.click();
		Thread.sleep(1000);
		System.out.println("fn_apply");	}
	

public static void fn_olGridView(String locator) throws InterruptedException {
	WebElement olGridView = driver.findElement(By.cssSelector(locator));
	olGridView.click();
	Thread.sleep(2000);
}

public static void fn_olListView(String locator) throws InterruptedException {
	WebElement olListView = driver.findElement(By.cssSelector(locator));
	olListView.click();
	Thread.sleep(2000);
	System.out.println("fn_olListView"); }

public static void fn_olViewDetails(String locator) throws InterruptedException {
	WebElement olViewDetails = driver.findElement(By.linkText(locator));
	olViewDetails.click();
	Thread.sleep(2000);
	System.out.println("fn_olViewDetails");
	}

public static void fn_olOfferCount(String locator) throws InterruptedException {
	WebElement olOfferCount = driver.findElement(By.cssSelector(locator));
	String oc = olOfferCount.getText();
	System.out.println(oc);
	Thread.sleep(2000);
	System.out.println("fn_olOfferCount");}

public static void fn_olapply(String locator) throws InterruptedException {
	
	/* WebElement olapply = driver.findElement(By.xpath(locator));
	olapply.click();*/
	driver.navigate().to("https://beta.rubique.com/search/home-loan");
	Thread.sleep(2000);
	System.out.println("fn_olapply");}

public static void fn_viewDetails(String locator) throws InterruptedException {
	WebElement viewDetails = driver.findElement(By.cssSelector(locator));
	viewDetails.click();
		Thread.sleep(2000);
		System.out.println("fn_viewDetails");}

public static void fn_checkEligibility(String locator) throws InterruptedException {
	
	WebElement checkEligibility = driver.findElement(By.cssSelector(locator));
	checkEligibility.click();
		Thread.sleep(2000);
		System.out.println("fn_checkEligibility");}

public static void fn_offerList() throws InterruptedException {
	fn_olGridView("i[class='fa fa-th GridView visible-md visible-lg']");
	fn_olListView("i[class='fa fa-bars ListView visible-md visible-lg']");
	fn_olViewDetails("View Details");
	fn_olViewDetails("View Details");
	fn_olOfferCount("span[class='offerNumber']");
	fn_olapply("//*[@id=user-onboarding]/div/div/div[1]/div/div/fieldset[2]/div[4]/div[1]/div/table/tbody/tr[1]/td[5]/div[1]/button/span");
	
}

public static void fn_steponeMobile(String locator , String mobile) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steponeMobile = driver.findElement(By.cssSelector(locator));
	steponeMobile.sendKeys(mobile);
	System.out.println("fn_steponeMobile");}

public static void fn_steponeEmailId(String locator , String EmailId) throws InterruptedException {
	WebElement steponeEmail_Id = driver.findElement(By.cssSelector(locator));
	steponeEmail_Id.sendKeys(EmailId);
	System.out.println("fn_steponeEmailId");}

public static void fn_cssContinue(String locator) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steponeContinue = driver.findElement(By.cssSelector(locator));
	steponeContinue.click();
	System.out.println("fn_cssContinue");}

public static void fn_steptwo_FullName(String locator,String FullName) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_FullName = driver.findElement(By.cssSelector(locator));
	// WebElement steptwo_FullName = driver.findElement(By.xpath(locator));
	steptwo_FullName.sendKeys(FullName);
	System.out.println("fn_steptwo_FullName");}

public static void fn_steptwo_Gender(String locator , String Gender) throws InterruptedException {
	Thread.sleep(2000);
	Integer index = Integer.parseInt(Gender);
	Select steptwo_Gender =  new Select(driver.findElement(By.cssSelector(locator)));
	steptwo_Gender.selectByIndex(index);
	System.out.println("fn_steptwo_Gender");}


public static void fn_steptwo_dob(String locator , String dob ) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_dob = driver.findElement(By.cssSelector(locator));
	steptwo_dob.sendKeys(dob);
	System.out.println("fn_steptwo_dob");}

/* public static void fn_steptwo_dobMonth(String locator , String dobMonth) throws InterruptedException {
	WebElement steptwo_dobMonth = driver.findElement(By.cssSelector(locator));
	steptwo_dobMonth.sendKeys(dobMonth);
	}

public static void fn_steptwo_dobYear(String locator , String dobYear ) throws InterruptedException {
	WebElement steptwo_dobYear = driver.findElement(By.cssSelector(locator));
	steptwo_dobYear.sendKeys(dobYear);
	}

public static void fn_steptwo_dobdate(String locator , String dobdate ) throws InterruptedException {
	WebElement steptwo_dobdate = driver.findElement(By.linkText(locator));
	steptwo_dobdate.sendKeys(dobdate);
	} */

public static void fn_steptwo_MaritalStatus(String locator , String MaritalStatus) throws InterruptedException {
	Thread.sleep(2000);
	Integer index = Integer.parseInt(MaritalStatus);
	Select steptwo_MaritalStatus =  new Select(driver.findElement(By.cssSelector(locator)));
	steptwo_MaritalStatus.selectByIndex(index);
	System.out.println("fn_steptwo_MaritalStatus");}

public static void fn_steptwo_Nationality(String locator , String Nationality) throws InterruptedException {
	Thread.sleep(2000);
	Integer index = Integer.parseInt(Nationality);
	Select steptwo_Nationality =  new Select(driver.findElement(By.cssSelector(locator)));
	steptwo_Nationality.selectByIndex(index);
	System.out.println("fn_steptwo_Nationality");}

public static void fn_steptwo_PAN(String locator , String PAN) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_PAN = driver.findElement(By.cssSelector(locator));
	steptwo_PAN.sendKeys(PAN);
	System.out.println("fn_steptwo_PAN");}

public static void fn_steptwo_PassportNo(String locator , String PassportNo) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_PassportNo = driver.findElement(By.cssSelector(locator));
	steptwo_PassportNo.sendKeys(PassportNo);
	System.out.println("fn_steptwo_PassportNo");}

public static void fn_steptwo_AadhaarNo(String locator , String AadhaarNo) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_AadhaarNo = driver.findElement(By.cssSelector(locator));
	steptwo_AadhaarNo.sendKeys(AadhaarNo);
	System.out.println("fn_steptwo_AadhaarNo");}

public static void fn_steptwo_AddressLine1(String locator , String AddressLine1) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_AddressLine1 = driver.findElement(By.cssSelector(locator));
	steptwo_AddressLine1.sendKeys(AddressLine1);
	System.out.println("fn_steptwo_AddressLine1");}

public static void fn_steptwo_AddressLine2(String locator , String AddressLine2) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_AddressLine2 = driver.findElement(By.cssSelector(locator));
	steptwo_AddressLine2.sendKeys(AddressLine2);
	System.out.println("fn_steptwo_AddressLine2");}

public static void fn_steptwo_Landmark(String locator , String Landmark) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_Landmark = driver.findElement(By.cssSelector(locator));
	steptwo_Landmark.sendKeys(Landmark);
	System.out.println("fn_steptwo_Landmark");}

public static void fn_city(String locator , String City) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(City);
	Select steptwo_City =  new Select(driver.findElement(By.cssSelector(locator)));
	steptwo_City.selectByIndex(index);
	System.out.println("fn_City");}

public static void fn_steptwo_Pincode(String locator , String Pincode) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_Pincode = driver.findElement(By.cssSelector(locator));
	steptwo_Pincode.sendKeys(Pincode);
	System.out.println("fn_steptwo_Pincode");}

public static void fn_steptwo_LandlineNumber(String locator , String LandlineNumber) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_LandlineNumber = driver.findElement(By.cssSelector(locator));
	steptwo_LandlineNumber.sendKeys(LandlineNumber);
	System.out.println("fn_steptwo_LandlineNumber");}

public static void fn_steptwo_YearsCurrentResidence(String locator , String YearsCurrentResidence) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_YearsCurrentResidence = driver.findElement(By.cssSelector(locator));
	steptwo_YearsCurrentResidence.sendKeys(YearsCurrentResidence);
	System.out.println("fn_steptwo_YearsCurrentResidence");}

public static void fn_steptwo_TypeofAccommodation(String locator , String TypeofAccommodation) throws InterruptedException {
	Thread.sleep(2000);
	Integer index = Integer.parseInt(TypeofAccommodation);
	Select steptwo_TypeofAccommodation =  new Select(driver.findElement(By.cssSelector(locator)));
	steptwo_TypeofAccommodation.selectByIndex(index);
	System.out.println("fn_steptwo_TypeofAccommodation");}

public static void fn_steptwo_permanentaddressLine1(String locator , String permanentaddressLine1) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_permanentaddressLine1 = driver.findElement(By.cssSelector(locator));
	steptwo_permanentaddressLine1.sendKeys(permanentaddressLine1);
	System.out.println("fn_steptwo_permanentaddressLine1");}

public static void fn_steptwo_permanentAddressLine2(String locator , String permanentAddressLine2) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_permanentAddressLine2 = driver.findElement(By.cssSelector(locator));
	steptwo_permanentAddressLine2.sendKeys(permanentAddressLine2);
	System.out.println("fn_steptwo_permanentAddressLine2");}

public static void fn_steptwo_permanentlandmark3(String locator , String permanentlandmark3) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_permanentlandmark3 = driver.findElement(By.cssSelector(locator));
	steptwo_permanentlandmark3.sendKeys(permanentlandmark3);
	System.out.println("fn_steptwo_permanentlandmark3");}

public static void fn_steptwo_permanentCity(String locator , String permanentCity) throws InterruptedException {
	Thread.sleep(2000);
	Integer index = Integer.parseInt(permanentCity);
	Select steptwo_permanentCity =  new Select(driver.findElement(By.cssSelector(locator)));
	steptwo_permanentCity.selectByIndex(index);
	System.out.println("fn_steptwo_permanentCity");}

public static void fn_steptwo_permanentPincode(String locator , String permanentPincode) throws InterruptedException {
	Thread.sleep(2000);
	WebElement steptwo_permanentPincode = driver.findElement(By.cssSelector(locator));
	steptwo_permanentPincode.sendKeys(permanentPincode);
	System.out.println("fn_steptwo_permanentPincode");}


public static void fn_stepthree_loanAmountRequired(String locator , String LoanAmount) throws InterruptedException {
	Thread.sleep(2000);
	WebElement LoanAmountRequired = driver.findElement(By.cssSelector(locator));
	LoanAmountRequired.sendKeys(LoanAmount);
	System.out.println("fn_stepthree_LoanAmountRequired");}

public static void fn_stepthree_tenureYears(String locator,String years) throws InterruptedException {
	WebElement TenureYears = driver.findElement(By.cssSelector(locator));
	TenureYears.sendKeys(years);
	System.out.println("fn_stepthree_TenureYears");}

public static void fn_stepthree_tenureMonths(String locator,String months) throws InterruptedException {
	WebElement TenureMonths = driver.findElement(By.cssSelector(locator));
	TenureMonths.sendKeys(months);
	System.out.println("fn_stepthree_TenureMonths");	}


public static void fn_stepfour_Occupation (String locator , String Occupation) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(Occupation);
	Select stepfour_Occupation =  new Select(driver.findElement(By.cssSelector(locator)));
	stepfour_Occupation.selectByIndex(index);
	System.out.println("fn_stepfour_Occupation");}

public static void fn_stepfour_sserCompanyName(String locator , String CompanyName) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_CompanyName = driver.findElement(By.cssSelector(locator));
	stepfour_CompanyName.sendKeys(CompanyName);
	System.out.println("fn_stepfour_CompanyName");}

public static void fn_stepfour_srGrossMonthlyIncome(String locator , String GrossMonthlyIncome) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_GrossMonthlyIncome = driver.findElement(By.cssSelector(locator));
	stepfour_GrossMonthlyIncome.sendKeys(GrossMonthlyIncome);
	System.out.println("fn_stepfour_GrossMonthlyIncome");}

public static void fn_stepfour_seLastYearAnnualIncome(String locator , String LastYearAnnualIncome) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_seLastYearAnnualIncome = driver.findElement(By.cssSelector(locator));
	stepfour_seLastYearAnnualIncome.sendKeys(LastYearAnnualIncome);
	System.out.println("fn_stepfour_seLastYearAnnualIncome");}

public static void fn_stepfour_seAnnualIncomeYr2(String locator , String seAnnualIncomeYr2) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_seAnnualIncomeYr2 = driver.findElement(By.cssSelector(locator));
	stepfour_seAnnualIncomeYr2.sendKeys(seAnnualIncomeYr2);
	System.out.println("fn_stepfour_seAnnualIncomeYr2");}

public static void fn_stepfour_seConstitution (String locator , String seConstitution) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(seConstitution);
	Select stepfour_seConstitution =  new Select(driver.findElement(By.cssSelector(locator)));
	stepfour_seConstitution.selectByIndex(index);
	System.out.println("fn_stepfour_seConstitution");}


public static void fn_stepfour_sModeofSalary(String locator , String ModeofSalary) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(ModeofSalary);
	Select stepfour_ModeofSalary =  new Select(driver.findElement(By.cssSelector(locator)));
	stepfour_ModeofSalary.selectByIndex(index);
	System.out.println("fn_stepfour_ModeofSalary");}

public static void fn_stepfour_sTypeofCompany(String locator , String TypeofCompany) throws InterruptedException {
	Integer index =Integer.parseInt(TypeofCompany);
	Select stepfour_TypeofCompany =  new Select(driver.findElement(By.cssSelector(locator)));
	stepfour_TypeofCompany.selectByIndex(index);
	System.out.println("fn_stepfour_TypeofCompany");}

public static void fn_stepfour_sseProfessionType(String locator , String ProfessionType) throws InterruptedException {
	Integer index =Integer.parseInt(ProfessionType);
	Select stepfour_ProfessionType =  new Select(driver.findElement(By.cssSelector(locator)));
	stepfour_ProfessionType.selectByIndex(index);
	System.out.println("fn_stepfour_ProfessionType");}

public static void fn_stepfour_sseNumberofYearsinCurrentWork(String locator , String NumberofYearsinCurrentWork) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_NumberofYearsinCurrentWork = driver.findElement(By.cssSelector(locator));
	stepfour_NumberofYearsinCurrentWork.sendKeys(NumberofYearsinCurrentWork);
	System.out.println("fn_stepfour_NumberofYearsinCurrentWork");}

public static void fn_stepfour_sseTotalNumberofYearsinWork(String locator , String TotalNumberofYearsinWork) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepfour_TotalNumberofYearsinWork = driver.findElement(By.cssSelector(locator));
	stepfour_TotalNumberofYearsinWork.sendKeys(TotalNumberofYearsinWork);
	System.out.println("fn_stepfour_TotalNumberofYearsinWork");}

public static void fn_stepfour_seFinancialsAudited(String locator , String seFinancialsAudited) throws InterruptedException {
	Integer index =Integer.parseInt(seFinancialsAudited);
	Select stepfour_seFinancialsAudited =  new Select(driver.findElement(By.cssSelector(locator)));
	stepfour_seFinancialsAudited.selectByIndex(index);
	System.out.println("fn_stepfour_seFinancialsAudited");}




public static void fn_stepfive_anyExistingLoan(String locator , String AnyExistingLoan) throws InterruptedException {
	Thread.sleep(5000);
	Integer index =Integer.parseInt(AnyExistingLoan);
	Select stepfive_AnyExistingLoan =  new Select(driver.findElement(By.cssSelector(locator)));
	stepfive_AnyExistingLoan.selectByIndex(index);
	System.out.println("fn_stepfive_AnyExistingLoan");	}

public static void fn_stepfive_existingLoanEMI(String locator , String ExistingLoanEMI) throws InterruptedException {
	Thread.sleep(5000);
	WebElement stepfive_ExistingLoanEMI = driver.findElement(By.cssSelector(locator));
	stepfive_ExistingLoanEMI.sendKeys(ExistingLoanEMI);
	System.out.println("fn_stepfive_ExistingLoanEMI");}

public static void fn_stepsix_AddCoapplicant(String locator , String AddCoapplicant) throws InterruptedException {
	Integer index =Integer.parseInt(AddCoapplicant);
	Select stepsix_AddCoapplicant =  new Select(driver.findElement(By.cssSelector(locator)));
	stepsix_AddCoapplicant.selectByIndex(index);
	System.out.println("fn_stepsix_AddCoapplicant");	}

public static void fn_stepsix_relationWithPrimaryApplicant(String locator , String relationWithPrimaryApplicant) throws InterruptedException {
	Integer index =Integer.parseInt(relationWithPrimaryApplicant);
	Thread.sleep(5000);
	Select stepsix_relationWithPrimaryApplicant =  new Select(driver.findElement(By.cssSelector(locator)));
	stepsix_relationWithPrimaryApplicant.selectByIndex(index);
	System.out.println("fn_stepsix_relationWithPrimaryApplicant");	}

public static void fn_stepsix_coapplicantName(String locator , String coapplicantName) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepsix_coapplicantName = driver.findElement(By.cssSelector(locator));
	stepsix_coapplicantName.sendKeys(coapplicantName);
	System.out.println("fn_stepsix_coapplicantName");}

public static void fn_stepsix_coapplicantDOB(String locator , String coapplicantDOB) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepsix_coapplicantDOB = driver.findElement(By.cssSelector(locator));
	stepsix_coapplicantDOB.sendKeys(coapplicantDOB);
	System.out.println("fn_stepsix_coapplicantDOB");}

public static void fn_stepsix_coapplicantEmail(String locator , String coapplicantEmail) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepsix_coapplicantEmail = driver.findElement(By.cssSelector(locator));
	stepsix_coapplicantEmail.sendKeys(coapplicantEmail);
	System.out.println("fn_stepsix_coapplicantEmail");}

public static void fn_stepsix_coapplicantMobile(String locator , String coapplicantMobile) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepsix_coapplicantMobile = driver.findElement(By.cssSelector(locator));
	stepsix_coapplicantMobile.sendKeys(coapplicantMobile);
	System.out.println("fn_stepsix_coapplicantMobile");}

public static void fn_stepsix_coapplicantOccupation(String locator , String coapplicantOccupation) throws InterruptedException {
	Thread.sleep(5000);
	Integer index =Integer.parseInt(coapplicantOccupation);
	Select stepsix_coapplicantOccupation =  new Select(driver.findElement(By.cssSelector(locator)));
	stepsix_coapplicantOccupation.selectByIndex(index);
	System.out.println("fn_stepsix_coapplicantOccupation");	}

public static void fn_stepsix_coapplicantMonthlyIncome(String locator , String coapplicantMonthlyIncome) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepsix_coapplicantMonthlyIncome = driver.findElement(By.cssSelector(locator));
	stepsix_coapplicantMonthlyIncome.sendKeys(coapplicantMonthlyIncome);
	System.out.println("fn_stepsix_coapplicantMonthlyIncome");}

public static void fn_stepsix_coapplicantExistingLoan(String locator , String coapplicantExistingLoan) throws InterruptedException {
	Thread.sleep(5000);
	Integer index =Integer.parseInt(coapplicantExistingLoan);
	Select stepfive_coapplicantExistingLoan =  new Select(driver.findElement(By.cssSelector(locator)));
	stepfive_coapplicantExistingLoan.selectByIndex(index);
	System.out.println("fn_stepfive_coapplicantExistingLoan");	}

public static void fn_stepsix_coapplicantExistingEMI(String locator , String coapplicantExistingEMI) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepsix_coapplicantExistingEMI = driver.findElement(By.cssSelector(locator));
	stepsix_coapplicantExistingEMI.sendKeys(coapplicantExistingEMI);
	System.out.println("fn_stepsix_coapplicantExistingEMI");}

public static void fn_stepseven_BankAccountType(String locator , String BankAccountType) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(BankAccountType);
	Select stepseven_BankAccountType =  new Select(driver.findElement(By.cssSelector(locator)));
	stepseven_BankAccountType.selectByIndex(index);
	System.out.println("fn_stepseven_BankAccountType");}

public static void fn_stepseven_BankingSinceYears(String locator,String BankingSinceYears) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepseven_BankingSinceYears = driver.findElement(By.xpath(locator));
	stepseven_BankingSinceYears.sendKeys(BankingSinceYears);
	System.out.println("fn_stepseven_BankingSinceYears");	}

public static void fn_stepseven_BankingSinceMonths(String locator , String BankingSinceMonths) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepseven_BankingSinceMonths = driver.findElement(By.xpath(locator));
	stepseven_BankingSinceMonths.sendKeys(BankingSinceMonths);
	System.out.println("fn_stepseven_BankingSinceMonths");}


public static void fn_stepseven_haveCurrentAccountsincelastoneyear(String locator , String haveCurrentAccountsincelastoneyear) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(haveCurrentAccountsincelastoneyear);
	Select stepseven_haveCurrentAccountsincelastoneyear =  new Select(driver.findElement(By.cssSelector(locator)));
	stepseven_haveCurrentAccountsincelastoneyear.selectByIndex(index);
	System.out.println("fn_stepseven_haveCurrentAccountsincelastoneyear");	}

public static void fn_stepseven_AnyChequeBounce(String locator , String AnyChequeBounce) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(AnyChequeBounce);
	Select fn_stepseven_AnyChequeBounce =  new Select(driver.findElement(By.cssSelector(locator)));
	fn_stepseven_AnyChequeBounce.selectByIndex(index);
	System.out.println("fn_stepseven_AnyChequeBounce");
	}

public static void fn_stepeight_TypeofProperty(String locator , String TypeofProperty) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(TypeofProperty);
	Select stepeight_TypeofProperty =  new Select(driver.findElement(By.cssSelector(locator)));
	stepeight_TypeofProperty.selectByIndex(index);
	System.out.println("fn_stepeight_TypeofProperty");}



public static void fn_stepeight_ClassificationofProperty(String locator , String ClassificationofProperty) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(ClassificationofProperty);
	Select stepeight_ClassificationofProperty =  new Select(driver.findElement(By.xpath(locator)));
	stepeight_ClassificationofProperty.selectByIndex(index);
	System.out.println("fn_stepeight_ClassificationofProperty");
	}

public static void fn_stepeight_PurposeofLoan(String locator , String PurposeofLoan) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(PurposeofLoan);
	Select stepeight_PurposeofLoan =  new Select(driver.findElement(By.xpath(locator)));
	stepeight_PurposeofLoan.selectByIndex(index);
	System.out.println("fn_stepeight_PurposeofLoan");}

public static void fn_stepeight_StatusofProperty(String locator , String  StatusofProperty) throws InterruptedException {
	Thread.sleep(3000);
	Integer index =Integer.parseInt(StatusofProperty);
	Select stepeight_StatusofProperty =  new Select(driver.findElement(By.cssSelector(locator)));
	stepeight_StatusofProperty.selectByIndex(index);
	System.out.println("fn_stepeight_StatusofProperty");}

public static void fn_stepeight_PropertyName(String locator , String PropertyName) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepeight_PropertyName = driver.findElement(By.xpath(locator));
	stepeight_PropertyName.sendKeys(PropertyName);
	System.out.println("fn_stepeight_PropertyName");} 


public static void fn_stepeight_BuilderDeveloper(String locator , String BuilderDeveloper) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepeight_BuilderDeveloper = driver.findElement(By.xpath(locator));
	stepeight_BuilderDeveloper.sendKeys(BuilderDeveloper);
	System.out.println("fn_stepeight_BuilderDeveloper");}


public static void fn_stepeight_PropertyCity(String locator , String PropertyCity) throws InterruptedException {
	Thread.sleep(2000);
	Integer index =Integer.parseInt(PropertyCity);
	Select stepeight_PropertyCity =  new Select(driver.findElement(By.xpath(locator)));
	stepeight_PropertyCity.selectByIndex(index);
	System.out.println("fn_stepeight_PropertyCity");}

public static void fn_stepeight_MarketValueOfProperty(String locator , String MarketValueOfProperty) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepeight_MarketValueOfProperty = driver.findElement(By.cssSelector(locator));
    stepeight_MarketValueOfProperty.sendKeys(MarketValueOfProperty);
    System.out.println("fn_stepeight_MarketValueOfProperty");}

public static void fn_stepeight_Submit(String locator ) throws InterruptedException {
	Thread.sleep(2000);
	WebElement stepeight_Submit = driver.findElement(By.xpath(locator));
	stepeight_Submit.click();
    System.out.println("fn_stepeight_Submit");}

public static void fn_TrackingID(String locator ) throws InterruptedException {
	Thread.sleep(2000);
	WebElement TrackingID = driver.findElement(By.xpath(locator));
	TrackingID.getText();
    System.out.println(TrackingID.getText());}


public static void fn_scrollPage(String locator) {
	WebElement element = driver.findElement(By.xpath(locator));

	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);

}

public static void fn_closeDriver() {
		driver.close();
		driver.quit();
	}
	


}

