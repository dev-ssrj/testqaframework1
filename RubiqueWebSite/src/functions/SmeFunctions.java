package functions;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SmeFunctions{
	
	
	static WebDriver driver;	
	 static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	   
	  public SmeFunctions(WebDriver driver) 
	  {
		  SmeFunctions.driver = driver;
	  }
	  
	  public SmeFunctions() 
	  {
	  }
	
		public WebDriver fn_setUpChrome() {

			workingDir = cromeDrivePath;
			System.out.println(workingDir);
			System.setProperty("webdriver.chrome.driver", cromeDrivePath);
				driver = new ChromeDriver();
			driver.manage().window().maximize();
			System.out.println("fn_setUpChrome");
			return driver;
		}
		
		public void fn_takeScreenshot(WebDriver driver, String path) {
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(src, new File(path));
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("fn_takeScreenshot taken check at: " + path);
		}

		public void fn_openUrl(String URL) {
			driver.get(URL);
				try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			System.out.println("fn_openUrl");

		}
		
		
		public void fn_homeLoginClick(String locator) 
		{
			WebElement Next = (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
			Next.click();
		}

		public void fn_emailId(String locator, String email) throws InterruptedException
		{
			Thread.sleep(1000);
			System.out.println("email" + email);
			System.out.println("locator" + locator);
			WebElement emailid = driver.findElement(By.cssSelector(locator));
			emailid.sendKeys(email);
		}

		public void fn_password(String locator, String pass) 
		{
			System.out.println("pass" + pass);
			System.out.println("locator" + locator);
			WebElement password = driver.findElement(By.cssSelector(locator));
			password.sendKeys(pass);
		}

		public void fn_login(String locator) 
		{
			System.out.println("locator" + locator);
			WebElement login = driver.findElement(By.cssSelector(locator));
			login.click();
		}

		
	  
	  public void fn_clickSME(String locator)
	  {
		  WebElement clickSME = driver.findElement(By.xpath(locator));
		  clickSME.click();
		  System.out.println("fn_clickSME");
	  }/*
	  public void fn_clickApplyHere(String locator)
	  {
		  WebElement clickApplyHere = driver.findElement(By.xpath(locator));
		  clickApplyHere.click();
		  System.out.println("fn_clickApplyHere");
	  }
	  
	  public void fn_EnterName(String Locator, String name) throws InterruptedException
	  {
		  Thread.sleep(2000);
		  WebElement EnterName = driver.findElement(By.xpath(Locator));
		  EnterName.sendKeys(name);
		  System.out.println("fn_EnterName");
	  }
	  
	  public void fn_EnterMail(String locator, String mail)
	  {
		  WebElement EnterMail = driver.findElement(By.xpath(locator));
		  EnterMail.sendKeys(mail);
		  System.out.println("fn_EnterMail");
	  }
	  
	  public void fn_EnterMobile(String locator, String mobile)
	  {
		  WebElement EnterMail = driver.findElement(By.xpath(locator));
		  EnterMail.sendKeys(mobile);
		  System.out.println("fn_EnterMobile");
	  }
	  
	  public void fn_EnterCity(String locator, String city)
	  {
		  WebElement EnterMail = driver.findElement(By.xpath(locator));
		  EnterMail.sendKeys(city);
		  System.out.println("fn_EnterCity");
	  }
	  
	  public void fn_SelectProduct(String locator, String product)
	  {
		  Select drop0 = new Select(driver.findElement(By.xpath(locator)));
			 drop0.selectByVisibleText(product);
			 System.out.println("fn_SelectProduct");
	  }
	  
	  public void fn_EnterComment(String locator, String Comment)
	  {
		  WebElement EnterComment = driver.findElement(By.xpath(locator));
		  EnterComment.sendKeys(Comment);
		  System.out.println("fn_EnterComment");
	  }
	  
	  public void fn_ClickSubmit(String locator)
	  {
		  WebElement EnterComment = driver.findElement(By.xpath(locator));
		  EnterComment.click();
		  System.out.println("fn_ClickSubmit");
	  }
	  
	  public void fn_ClickClosebutton(String locator) throws InterruptedException
	  {
		  Thread.sleep(2000);
		  WebElement EnterComment = driver.findElement(By.xpath(locator));
		  EnterComment.click();
		  System.out.println("fn_ClickClosebutton");
	  }*/
	  

	  public void fn_ClickSMBOveriview(String locator)
	  {
		  WebElement fn_ClickSMBOveriview = driver.findElement(By.xpath(locator));
		  fn_ClickSMBOveriview.click();
	   }
	  
	  public void fn_EnterLaonAmount(String locator, String LoanA) throws InterruptedException
		{
			Thread.sleep(1000);
			WebElement fn_EnterLaonAmount = driver.findElement(By.cssSelector(locator));
			fn_EnterLaonAmount.sendKeys(LoanA);
			
		}
	
	  public void fn_EnterCompanyName(String locator, String LoanA) throws InterruptedException
		{
			Thread.sleep(1000);
			WebElement fn_EnterCompanyName = driver.findElement(By.cssSelector(locator));
			fn_EnterCompanyName.sendKeys(LoanA);
		}  
	  
	  public void fn_EnterMailId(String locator, String mail)
	  {
		  WebElement fn_EnterMailId = driver.findElement(By.cssSelector(locator));
		  fn_EnterMailId.sendKeys(mail);
	  }
	  
	  public void fn_EnterMobileNo(String locator, String no)
	  {
		  WebElement fn_EnterMobileNo = driver.findElement(By.cssSelector(locator));
		  fn_EnterMobileNo.sendKeys(no);
	  }
	  
	  public void fn_SelectCity(String locator, String City) throws InterruptedException {
			Thread.sleep(500);
			Select city = new Select(driver.findElement(By.cssSelector(locator)));
			city.selectByVisibleText(City);
		}
	  
	  public void fn_ClickGetStarted(String locator)
	  {
		  WebElement fn_ClickGetStarted = driver.findElement(By.cssSelector(locator));
		  fn_ClickGetStarted.click();
	   }
 
	  public void fn_scrollPage(String locator) throws InterruptedException {
		  Thread.sleep(1500);
			WebElement element = driver.findElement(By.xpath(locator));

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			}
	  
	  public void fn_SelectDOI(String locator, String pss) throws InterruptedException {
			Thread.sleep(500);
			WebElement fn_SelectDOI = driver.findElement(By.cssSelector(locator));
			fn_SelectDOI.sendKeys(pss);
		}
	  
	  public void fn_SelectConstitution(String locator, String City) throws InterruptedException {
			Thread.sleep(500);
			Select city = new Select(driver.findElement(By.cssSelector(locator)));
			city.selectByVisibleText(City);
		}
	  
	  public void fn_EnterPAN(String locator, String no)
	  {
		  WebElement fn_EnterPAN = driver.findElement(By.cssSelector(locator));
		  fn_EnterPAN.sendKeys(no);
	  }
	  
	  public void fn_ClickNext(String locator)
	  {
		  WebElement fn_ClickNext = driver.findElement(By.cssSelector(locator));
		  fn_ClickNext.click();
	   }
	  
	  public void fn_EnterName(String locator, String name) throws InterruptedException
	  {
		  Thread.sleep(6000);
		  WebElement fn_EnterName = driver.findElement(By.xpath(locator));
		  fn_EnterName.sendKeys(name);
	  }
	  
	  public void fn_EnterMobile(String locator, String no)
	  {
		  WebElement fn_EnterMobile = driver.findElement(By.xpath(locator));
		  fn_EnterMobile.sendKeys(no);
	  }
	  
	  public void fn_EnterEmail(String locator, String email)
	  {
		  WebElement fn_EnterEmail = driver.findElement(By.xpath(locator));
		  fn_EnterEmail.sendKeys(email);
	  }
	  
	  public void fn_SelectCity1(String locator, String City) throws InterruptedException {
			Thread.sleep(500);
			Select city = new Select(driver.findElement(By.xpath(locator)));
			city.selectByVisibleText(City);
		}
	  
	  public void fn_ClickContinue1(String locator)
	  {
		  WebElement fn_ClickContinue1 = driver.findElement(By.xpath(locator));
		  fn_ClickContinue1.click();
	  }
	  
	  public void fn_SelectSegment(String locator, String Segment) throws InterruptedException {
			Thread.sleep(500);
			Select Segment1 = new Select(driver.findElement(By.xpath(locator)));
			Segment1.selectByVisibleText(Segment);
		}
	  
	  public void fn_EnterNOofYrswithPartners(String locator, String yrs)
	  {
		  WebElement fn_EnterNOofYrswithPartners = driver.findElement(By.xpath(locator));
		  fn_EnterNOofYrswithPartners.sendKeys(yrs);
	  }
	  
	  public void fn_EnterNoOfTranscation(String locator, String no)
	  {
		  WebElement fn_EnterNoOfTranscation = driver.findElement(By.xpath(locator));
		  fn_EnterNoOfTranscation.sendKeys(no);
	 }

	  public void fn_EnterpercentagethorPartner(String locator, String perct)
	  {
		  WebElement fn_EnterpercentagethorPartner = driver.findElement(By.xpath(locator));
		  fn_EnterpercentagethorPartner.sendKeys(perct);
	 }
     
	  public void fn_SelectDefaultwithPartner(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_EnterBookingtoCancellation(String locator, String perct)
	  {
		  WebElement fn_EnterBookingtoCancellation = driver.findElement(By.xpath(locator));
		  fn_EnterBookingtoCancellation.sendKeys(perct);
	 }
	  
	  public void fn_SelectTypeofOperation(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_SelectEscrowAccount(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_SelectDepositwithPartner(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_SelectPartner(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_ClickContinue2(String locator)
	  {
		  WebElement fn_ClickContinue2 = driver.findElement(By.xpath(locator));
		  fn_ClickContinue2.click();
	  }
//---------------------------------------------
	  
	  public void fn_EnterFullName(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
		}
	  
	  public void fn_SelectDOB(String locator, String pss) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_SelectDOB = driver.findElement(By.xpath(locator));
			fn_SelectDOB.sendKeys(pss);
		}
	  
	  public void fn_SelectGender(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_EnterPAN1(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
		}
	  
	  public void fn_EnterMobile1(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
			}
	  
	  public void fn_EnterEmailid1(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
		}
	  
	  public void fn_Entershareperctg(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
		}
	  
	  public void fn_EnterAdress1(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
		}
	  
	  public void fn_EnterAdress2(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
		}
	  
	  public void fn_EnterAdress3(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
		}
	  
	  public void fn_SelectCity11(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_EnterPincode(String locator, String pass) throws InterruptedException {
			Thread.sleep(1000);
			WebElement fn_EnterFullName = driver.findElement(By.xpath(locator));
			fn_EnterFullName.sendKeys(pass);
		}
	  
	  public void fn_SelectResidenceType(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_SelectOfficeType(String locator, String past)
	  {
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_ClickContinue3(String locator)
	  {
		  WebElement fn_ClickContinue1 = driver.findElement(By.xpath(locator));
		  fn_ClickContinue1.click();
	  }
	  
	  public void fn_SelectAnyExistingLoanwithBank(String locator, String past) throws InterruptedException
	  {
		  Thread.sleep(1000);
		  Select Past = new Select(driver.findElement(By.xpath(locator)));
		  Past.selectByVisibleText(past);
	  }
	  
	  public void fn_clickSubmit(String locator) throws InterruptedException {
			Thread.sleep(1000);
			WebElement clickNext = driver.findElement(By.cssSelector(locator));
			clickNext.click();
		}
}

