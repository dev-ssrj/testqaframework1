
package functions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import drivers.CvDrivers;


public class CvFunctions extends CvDrivers{
	public static String workingDir = System.getProperty("user.dir");
	public static String cromeDrivePath = workingDir + "/utility/chromedriver.exe";
	public static String inputFileTWL1 = workingDir + "/RubiqueWebSite/inputFiles/twl_productflow_20170620_pk_01.xls";
	//----------------------------need to be change
	public static WebDriver driver;
	public static String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	public static String screenSnapsPath = workingDir +"/outputFiles/"+"twl"+timeStamp+".png";

	
	
	public  static  void fn_takeScreenshot(WebDriver driver, String path) {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(src, new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("fn_takeScreenshot taken check at: "+path);
	}
	public WebDriver fn_setUpChrome() {

		workingDir = cromeDrivePath;
		System.out.println(workingDir);
		System.setProperty("webdriver.chrome.driver", cromeDrivePath);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("fn_setUpChrome");
		return driver;
	    }

	public void fn_openUrl(String URL) {
		driver.get(URL);
		System.out.println("fn_openUrl");
	}
	public void fn_homeLoginClick(String locator) throws InterruptedException  
	{
		WebElement homeLoginClick = driver.findElement(By.xpath(locator));
		homeLoginClick.click();
		System.out.println("fn_homeLoginClick");
	}
	public void fn_emailId(String locator, String email) throws InterruptedException {
		Thread.sleep(2000);
		WebElement emailid = driver.findElement(By.xpath(locator));
		emailid.sendKeys(email);
		System.out.println("fn_emailId");
	}
	public void fn_password(String locator, String pss) throws InterruptedException {
		WebElement passwrd = driver.findElement(By.xpath(locator));
		passwrd.sendKeys(pss);
		System.out.println("fn_password");
	}
	public void fn_login(String string) throws InterruptedException {
		WebElement login = driver.findElement(By.xpath(string));
		login.click();
		login.click();
		System.out.println("fn_login");
	}
	public void fn_scrollPage(String locator) throws InterruptedException {
		Thread.sleep(2000);
		WebElement element = driver.findElement(By.xpath(locator));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		}
public void fn_FooterProduct(String locator) throws InterruptedException{
	Thread.sleep(2000);
	WebElement passwrd = driver.findElement(By.xpath(locator));
	Actions actions = new Actions(driver);
	actions.moveToElement(passwrd).click().perform();
	System.out.println("fn_FooterProduct");
	}
public void fn_scrollpage()
{

JavascriptExecutor jse = (JavascriptExecutor)driver;
jse.executeScript("window.scrollBy(0,250)", "");
}
public void fn_ViewDeatils(String locator) throws InterruptedException //click on homepage login
{Thread.sleep(4000);
	WebElement homeLoginClick = driver.findElement(By.cssSelector(locator));
	homeLoginClick.click(); 
	System.out.println("fn_ViewDeatils");
}
public void fn_clickEligibility(String locator) throws InterruptedException //click on homepage login
{Thread.sleep(2000);
	WebElement homeLoginClick = driver.findElement(By.cssSelector(locator));
	Thread.sleep(2000);
	homeLoginClick.click(); 
	
	System.out.println("fn_clickEligibility");
}

public void fn_selectFirstStageProductMatch(String locator) throws InterruptedException{
	Thread.sleep(2000);
	WebDriverWait wait = new WebDriverWait(driver,30);
wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));  
driver.findElement(By.cssSelector(locator)).click();

    }
public void fn_selectSecondStagemobile(String locator,String pss) throws InterruptedException {//filling pwd
	Thread.sleep(4000);
	WebElement passwrd = driver.findElement(By.cssSelector(locator));
	passwrd.clear();
	passwrd.sendKeys(pss);
	System.out.println("fn_selectSecondStagemobile");
	}
public void fn_selectSecondStageEmailId(String locator,String pss) throws InterruptedException {//filling pwd
	Thread.sleep(1000);

	WebElement passwrd = driver.findElement(By.cssSelector(locator));
	passwrd.clear();
	passwrd.sendKeys(pss);
	System.out.println("fn_selectSecondStageEmailId");
	}
public void fn_stepTwoSelectCity(String locator,String Owned){
	 Select drp3 = new Select(driver.findElement(By.cssSelector(locator)));
		drp3.selectByVisibleText(Owned);
		System.out.println("fn_stepTwoSelectCity");
}
public void fn_selectSecondStageNext(String locator) throws InterruptedException {//filling pwd
	Thread.sleep(2000);
	WebElement passwrd = driver.findElement(By.xpath(locator));
	passwrd.click();;
	System.out.println(" fn_selectSecondStageNext");
	}

public void fn_completeSecondStepFullName(String locator,String pass) throws InterruptedException{
	Thread.sleep(2000);
	WebElement completeSecondStepFullName=driver.findElement(By.cssSelector(locator));
	completeSecondStepFullName.sendKeys(pass);
	System.out.println("Pass FullName");	
}
public void fn_stepTwoSelectGenderId(String locator,String gender) throws InterruptedException {
	Thread.sleep(2000);
	Select drp0 = new Select(driver.findElement(By.cssSelector(locator)));
	drp0.selectByVisibleText(gender);
	System.out.println("Pass Gender");

}
 public void fn_stepTwoSelectDOB(String locator,String pss) throws InterruptedException{
	 Thread.sleep(2000);
	 WebElement stepTwoSelectDOB = driver.findElement(By.cssSelector(locator));
	 stepTwoSelectDOB.sendKeys(pss);	
		System.out.println("Pass DOB");

 }
 public void fn_stepTwoSelectMarital(String locator,String Marital) throws InterruptedException{
	 Thread.sleep(2000);
	 Select drp1 = new Select(driver.findElement(By.cssSelector(locator)));
		drp1.selectByVisibleText(Marital);
		System.out.println("Pass MaritalStatus");

 }
 public void fn_stepTwoSelectNationality(String locator,String Nationality) throws InterruptedException{
	 Thread.sleep(1000);
	 Select drp2 = new Select(driver.findElement(By.cssSelector(locator)));
		drp2.selectByVisibleText(Nationality);
		System.out.println("Pass Nationality");

 }
 public void fn_completeSecondStagePan(String locator,String pann) throws InterruptedException{ 		
	 Thread.sleep(2000);
	 WebElement completeSecondStagePan = driver.findElement(By.cssSelector(locator));
		completeSecondStagePan.sendKeys(pann);	
		System.out.println("Pass PAN_ID");

	} 
 public void fn_stepTwoSelectAddressLine1(String locator,String pss) throws InterruptedException
 {	 Thread.sleep(1000);

	 WebElement stepTwoSelectAddressLine1= driver.findElement(By.cssSelector(locator));
	 stepTwoSelectAddressLine1.sendKeys(pss);
		System.out.println("Pass Address 1st");

 }
 public void fn_stepTwoSelectAddressLine2(String locator,String pss) throws InterruptedException
 {	 Thread.sleep(1000);

	 WebElement stepTwoSelectAddress_Line_2= driver.findElement(By.cssSelector(locator));
	 stepTwoSelectAddress_Line_2.sendKeys(pss);
		System.out.println("Pass Address 2st");

 }
 
 public void fn_stepTwoSelectPincode(String locator,String pss){
	 WebElement stepTwoSelectPincode = driver.findElement(By.cssSelector(locator));
	 stepTwoSelectPincode.sendKeys(pss);
		System.out.println("Pass PinCode");
 }
 public void fn_stepTwoSelectYearsatcurrentresidence(String locator,String ps){
	 WebElement Years_at_current_residence = driver.findElement(By.cssSelector(locator));
	 Years_at_current_residence.sendKeys(ps);
		System.out.println("Pass Years_at_current_residence"); 
 }
 public void fn_stepTwoSelectTypeofAccommodation(String locator,String Owned) throws InterruptedException{
	 Thread.sleep(2000);
	 Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Owned);
		System.out.println("Pass Type_of_Accommodation");
 }  
 public void fn_stepTwoSelectContinue(String locator){
	 WebElement stepTwoSelectPincode = driver.findElement(By.xpath(locator));
	 stepTwoSelectPincode.click();
	System.out.println("fn_stepTwoSelectContinue Completed");
 }
 
 public void fn_completeThirdStageLoan_Amount_Required(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
		completeThirdStageLoan_Amount_Required.clear();
		Thread.sleep(1000);
		completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("Pass LoanAmountRequired");
	}
	public void fn_completeThirdStageTenure(String locator, String ps) throws InterruptedException {
		 Thread.sleep(1000);

		WebElement completeThirdStageTenure = driver.findElement(By.xpath(locator));
		completeThirdStageTenure.clear();
		Thread.sleep(1000);
		completeThirdStageTenure.sendKeys(ps);
		System.out.println("Pass Tenure");
	}
	public void fn_completeThirdStageContinue(String locator) throws InterruptedException {
		 Thread.sleep(1000);

		WebElement completeThirdStageContinue = driver.findElement(By.xpath(locator));
		completeThirdStageContinue.click();
		System.out.println("fn_completeThirdStage Completed");
	}
	public void fn_completeFourthStageOccupation(String locator, String Occupation) throws InterruptedException {
		Thread.sleep(1000);
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Occupation);
		System.out.println("Pass Occupation");
	}

	public void fn_completeFourthStageApplicantType(String locator, String Occupation) throws InterruptedException {
		Thread.sleep(1000);
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Occupation);
		System.out.println("fn_completeFourthStageApplicantType");
	}
	public void fn_completeFourthStageCompanyName(String locator, String ps) throws InterruptedException {
		 Thread.sleep(1000);

		WebElement completeThirdStageTenure = driver.findElement(By.cssSelector(locator));
		completeThirdStageTenure.clear();
		Thread.sleep(1000);
		completeThirdStageTenure.sendKeys(ps);
		System.out.println("fn_completeFourthStageCompanyName");
	}
	public void fn_completeFourthStageLastYearAnnualIncome(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
		completeThirdStageLoan_Amount_Required.clear();
		Thread.sleep(1000);
		completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("fn_completeFourthStageLastYearAnnualIncome");
	}
	public void fn_completeFourthStageConstitution(String locator, String Occupation) throws InterruptedException {
		Thread.sleep(1000);
		Select drp4 = new Select(driver.findElement(By.cssSelector(locator)));
		drp4.selectByVisibleText(Occupation);
		System.out.println("Pass fn_completeFourthStageConstitution");
	}
	public void fn_completeFourthStageAnnualIncomeYr2(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
		completeThirdStageLoan_Amount_Required.clear();
		Thread.sleep(1000);
		completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("Pass fn_completeFourthStageAnnualIncomeYr2");
	}
	public void fn_completeFourthStageTotalYearsinCurrentBusiness(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
		completeThirdStageLoan_Amount_Required.clear();
		Thread.sleep(1000);
		completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("Pass fn_completeFourthStageTotalYearsinCurrentBusiness");
	}
	public void fn_completeFourthStageTotalYearsinOverallBusiness(String locator, String ps) throws InterruptedException {
		Thread.sleep(1000);
		WebElement completeThirdStageLoan_Amount_Required = driver.findElement(By.cssSelector(locator));
		completeThirdStageLoan_Amount_Required.clear();
		Thread.sleep(1000);
		completeThirdStageLoan_Amount_Required.sendKeys(ps);
		System.out.println("Pass fn_completeFourthStageTotalYearsinOverallBusiness");
	}
	 public void fn_completeForthStageContinue(String locator){
		 WebElement stepTwoSelectPincode = driver.findElement(By.xpath(locator));
		 stepTwoSelectPincode.click();
		System.out.println("fn_stepTwoSelectContinue Completed");
	 }
	
	 public void fn_completeFifthStageAny_Existing_Loan_with_Bank(String locator, String No) throws InterruptedException {
			Thread.sleep(3000);
			Select drp6 = new Select(driver.findElement(By.cssSelector(locator)));
			drp6.selectByVisibleText(No);
			System.out.println("fn_completeFifthStageAny_Existing_Loan_with_Bank");
		}

		public void fn_completeFifthStageContinue(String locator) throws InterruptedException {
			WebElement completeFourthStageContinue = driver.findElement(By.xpath(locator));
			completeFourthStageContinue.click();
			System.out.println("fn_completeFifthStageContinue");
		}
		public void fn_completeSixthStagePrimary_Existing_Bank_Name(String locator, String Bank_name) throws InterruptedException {
			Thread.sleep(1000);
			Select drp8 = new Select(driver.findElement(By.cssSelector(locator)));
			drp8.selectByVisibleText(Bank_name);
			System.out.println("fn_completeSixthStagePrimary_Existing_Bank_Name");
		}

		public void fn_completeSixthStageBanking_Since(String locator, String year) throws InterruptedException {
			Thread.sleep(10000);
			WebElement completeSixthStagePrimary_Existing_Bank_Name = driver.findElement(By.xpath(locator));
			completeSixthStagePrimary_Existing_Bank_Name.sendKeys(year);
		}
		
		
	public void fn_completeFinalSubmit(String locator) {
		WebElement completeFinalSubmit = driver.findElement(By.cssSelector(locator));
		completeFinalSubmit.click();
		System.out.println("fn_completeFinalSubmit Completed");
	}
public void fn_MiddleProduct(String locator) throws InterruptedException{
	Thread.sleep(4000);
	WebElement passwrd = driver.findElement(By.xpath(locator));
	Actions actions = new Actions(driver);
	actions.moveToElement(passwrd).click().perform();
	System.out.println("MiddleProduct");
	}

public void fn_selectFirstStageLoanAmount(String locator, String pss) throws InterruptedException {
	Thread.sleep(1000);
	WebElement passwrd = driver.findElement(By.cssSelector(locator));
	passwrd.sendKeys(pss);
	System.out.println("Pass Password");
	Thread.sleep(1000);
}

public void fn_selectFirstStageEmploymentType(String locator){
	WebElement passwrd = driver.findElement(By.cssSelector(locator));
	passwrd.click();
	System.out.println("MiddleProduct");
	}

public void fn_selectFirstStageAnnualIncome(String locator, String pss) throws InterruptedException {
	WebElement passwrd = driver.findElement(By.cssSelector(locator));
	passwrd.sendKeys(pss);
	System.out.println("Pass Password");
}

public void fn_selectFirstStageTenure(String locator, String pss) throws InterruptedException {
	WebElement passwrd = driver.findElement(By.cssSelector(locator));
	passwrd.sendKeys(pss);
	System.out.println("Pass Password");
}

public void fn_selectFirstStageNext(String locator){
	WebElement passwrd = driver.findElement(By.xpath(locator));
	passwrd.click();
	System.out.println("MiddleProduct");
	}

}